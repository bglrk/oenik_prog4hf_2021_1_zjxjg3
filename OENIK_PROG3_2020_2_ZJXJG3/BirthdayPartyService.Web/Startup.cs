// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BirthdayPartyService.Data.Models;
    using BirthdayPartyService.Logic;
    using BirthdayPartyService.Repo;
    using BirthdayPartyService.Web.Models;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    /// <summary>
    /// This is the Startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The parameter is a configuration object.</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        /// <summary>
        /// This is the configureservices method.
        /// </summary>
        /// <param name="services">The parameter is a servicecollection object.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddSingleton<IMapper>((provider) => MapperFactory.CreateMapper());
            services.AddScoped<DbContext, BirthdayPartyServiceDatabaseContext>();
            services.AddScoped<IOrdersRepository, OrdersRepository>();
            services.AddScoped<ICustomersRepository, CustomersRepository>();
            services.AddScoped<IClownsRepository, ClownsRepository>();
            services.AddScoped<IServicesRepository, ServicesRepository>();
            services.AddScoped<IConnectorRepository, ConnectorRepository>();
            services.AddScoped<ICompanyLogic, CompanyLogic>();
            services.AddScoped<IVisibleContentLogic, VisibleContentLogic>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.

        /// <summary>
        /// This is the configure menthod.
        /// </summary>
        /// <param name="app">The parameter is an applicationbuilder object.</param>
        /// <param name="env">The parameter is a webhostenvironment object.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
