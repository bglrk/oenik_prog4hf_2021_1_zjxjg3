﻿// <copyright file="BirthdayServicesController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BirthdayPartyService.Data.Models;
    using BirthdayPartyService.Logic;
    using BirthdayPartyService.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// This is the controller class.
    /// </summary>
    public class BirthdayServicesController : Controller
    {
        private ICompanyLogic companyLogic;
        private IVisibleContentLogic visibleLogic;
        private IMapper mapper;
        private BirthdayServiceViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="BirthdayServicesController"/> class.
        /// </summary>
        /// <param name="companyLogic">The parameter is an icompanylogic object.</param>
        /// <param name="visibleLogic">The parameter is an ivisiblecontent object.</param>
        /// <param name="mapper">The parameter is an imapper object.</param>
        public BirthdayServicesController(ICompanyLogic companyLogic, IVisibleContentLogic visibleLogic, IMapper mapper)
        {
            this.companyLogic = companyLogic;
            this.visibleLogic = visibleLogic;
            this.mapper = mapper;

            this.vm = new BirthdayServiceViewModel();
            this.vm.EditedService = new Models.BirthdayService();

            var birthdayservices = visibleLogic.GetAllServices();
            this.vm.ListOfServices = mapper.Map<IList<Data.Models.ServiceOptions>, List<Models.BirthdayService>>(birthdayservices);
        }

        /// <summary>
        /// This is an Index method.
        /// </summary>
        /// <returns>Returns a view.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("BirthdayServicesIndex", this.vm);
        }

        /// <summary>
        /// This is the details method.
        /// </summary>
        /// <returns>Returns a view.</returns>
        /// <param name="id">The parameter is the id.</param>
        public IActionResult Details(int id)
        {
            return this.View("BirthdayServicesDetails", this.GetBirthdayServiceModel(id));
        }

        /// <summary>
        /// This is the remove method.
        /// </summary>
        /// <returns>Returns a view.</returns>
        /// <param name="id">The parameter is the id.</param>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAIL";
            if (this.companyLogic.DeleteServiceBool(id))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// This is the edit method.
        /// </summary>
        /// <returns>Returns a view.</returns>
        /// <param name="id">The parameter is the id.</param>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedService = this.GetBirthdayServiceModel(id);
            return this.View("BirthdayServicesIndex", this.vm);
        }

        /// <summary>
        /// This is another edit method.
        /// </summary>
        /// <param name="bservice">The parameter is a birthdayservice object.</param>
        /// <param name="editAction">The parameter is a string.</param>
        /// <returns>Returns a view.</returns>
        [HttpPost]
        public IActionResult Edit(Models.BirthdayService bservice, string editAction)
        {
            if (this.ModelState.IsValid && bservice != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        this.companyLogic.InsertNewService(bservice.ServiceName, bservice.Price, bservice.MinAge, bservice.MaxAge, bservice.Recommendation, bservice.IsIndoor);
                    }
                    catch (FormatException ex)
                    {
                        this.TempData["editResult"] = "Edit FAIL:" + ex.Message;
                    }
                }
                else
                {
                    ServiceOptions newservice = new ServiceOptions();
                    newservice.Id = bservice.Id;
                    newservice.Name = bservice.ServiceName;
                    newservice.Price = bservice.Price;
                    newservice.RecommendedMinAge = bservice.MinAge;
                    newservice.RecommendedMaxAge = bservice.MaxAge;
                    newservice.Indoor = bservice.IsIndoor;
                    newservice.Recommendation = bservice.Recommendation;

                    if (!this.companyLogic.ChangeServiceFullBool(newservice.Id, newservice))
                    {
                        this.TempData["editResult"] = "Edit FAIL";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.TempData["editResult"] = "Edit";
                this.vm.EditedService = bservice;
                return this.View("BirthdayServicesIndex", this.vm);
            }
        }

        private Models.BirthdayService GetBirthdayServiceModel(int id)
        {
            Data.Models.ServiceOptions oneService = this.visibleLogic.GetOneService(id);
            return this.mapper.Map<Data.Models.ServiceOptions, Models.BirthdayService>(oneService);
        }
    }
}
