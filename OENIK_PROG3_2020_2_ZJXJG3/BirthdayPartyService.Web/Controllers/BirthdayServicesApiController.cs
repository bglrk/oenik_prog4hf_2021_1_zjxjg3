﻿// <copyright file="BirthdayServicesApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BirthdayPartyService.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// This is the apicontroller class for the 3rd homework.
    /// </summary>
    public class BirthdayServicesApiController : Controller
    {
        private IMapper mapper;
        private ICompanyLogic compLogic;
        private IVisibleContentLogic visLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="BirthdayServicesApiController"/> class.
        /// </summary>
        /// <param name="mapper">The parameter is an IMapper object.</param>
        /// <param name="compLogic">The parameter is an ICompanyLogic object.</param>
        /// <param name="visLogic">The parameter is an IVisibleLogic object.</param>
        public BirthdayServicesApiController(IMapper mapper, ICompanyLogic compLogic, IVisibleContentLogic visLogic)
        {
            this.mapper = mapper;
            this.compLogic = compLogic;
            this.visLogic = visLogic;
        }

        // GET BirthdayServicesApi/all

        /// <summary>
        /// This is the GetAll method.
        /// </summary>
        /// <returns>Returns an IEnumerable list.</returns>
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.BirthdayService> GetAll()
        {
            var services = this.visLogic.GetAllServices();
            return this.mapper.Map<IList<Data.Models.ServiceOptions>, List<Models.BirthdayService>>(services);
        }

        // GET BirthdayServicesApi/del/5

        /// <summary>
        /// This is the DelOneService method.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <returns>Returns an ApiResult.</returns>
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneService(int id)
        {
            return new ApiResult() { OperationResult = this.compLogic.DeleteServiceBool(id) };
        }

        // POST BirthdayServicesApi/add + service

        /// <summary>
        /// This is the AddOneService method.
        /// </summary>
        /// <param name="service">The parameter is a service.</param>
        /// <returns>Returns an ApiResult.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneService(Models.BirthdayService service)
        {
            bool success = true;
            try
            {
                this.compLogic.InsertNewService(service.ServiceName, service.Price, service.MinAge, service.MaxAge, service.Recommendation, service.IsIndoor);
            }
            catch (FormatException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        // POST BirthdayServicesApi/mod  + service

        /// <summary>
        /// This is the ModOneService method.
        /// </summary>
        /// <param name="service">The parameter is a service object.</param>
        /// <returns>Returns an ApiResult.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneService(Models.BirthdayService service)
        {
            return new ApiResult()
            {
                OperationResult = this.compLogic.ChangeServiceFullBool2(service.Id, service.ServiceName, service.Price, service.MinAge, service.MaxAge, service.Recommendation, service.IsIndoor),
            };
        }
    }
}
