﻿// <copyright file="BirthdayServiceViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// This is the viewmodel class.
    /// </summary>
    public class BirthdayServiceViewModel
    {
        /// <summary>
        /// Gets or sets the edited service instance.
        /// </summary>
        public BirthdayService EditedService { get; set; }

        /// <summary>
        /// Gets or sets the list of services.
        /// </summary>
        public List<BirthdayService> ListOfServices { get; set; }
    }
}
