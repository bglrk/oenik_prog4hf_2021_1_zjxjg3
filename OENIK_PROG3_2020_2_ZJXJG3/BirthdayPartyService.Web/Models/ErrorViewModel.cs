// <copyright file="ErrorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Web.Models
{
    using System;

    /// <summary>
    /// This is the errorviewmodel class.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Gets or sets the requestid.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets a value indicating whether the stuff is true or not.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
