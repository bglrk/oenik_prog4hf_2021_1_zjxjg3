﻿// <copyright file="BirthdayService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// This is the BirthdayService class.
    /// </summary>
    public class BirthdayService
    {
        /// <summary>
        /// Gets or sets the ID of the service.
        /// </summary>
        [Display(Name = "Service id")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the service.
        /// </summary>
        [Display(Name = "Service name")]
        [Required]
        [StringLength(100)]
        public string ServiceName { get; set; }

        /// <summary>
        /// Gets or sets the price of the service.
        /// </summary>
        [Display(Name = "Service price")]
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets the recommended min age of the service.
        /// </summary>
        [Display(Name = "Recommended minimum age")]
        [Required]
        public int MinAge { get; set; }

        /// <summary>
        /// Gets or sets the recommended max age of the service.
        /// </summary>
        [Display(Name = "Recommended maximum age")]
        [Required]
        public int MaxAge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the service can be held indoor or not.
        /// </summary>
        [Display(Name = "Is it indoor?")]
        [Required]
        public bool IsIndoor { get; set; }

        /// <summary>
        /// Gets or sets the recommendation of the service.
        /// </summary>
        [Display(Name = "Service recommendation")]
        [Required]
        [Range(1, 10)]
        public int Recommendation { get; set; }
    }
}
