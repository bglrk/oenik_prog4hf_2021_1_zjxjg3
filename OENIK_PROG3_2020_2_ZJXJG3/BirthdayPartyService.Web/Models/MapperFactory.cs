﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// This is the mapper factory class.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// This is the createmapper method.
        /// </summary>
        /// <returns>Returns the configuration.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BirthdayPartyService.Data.Models.ServiceOptions, BirthdayPartyService.Web.Models.BirthdayService>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                    ForMember(dest => dest.ServiceName, map => map.MapFrom(src => src.Name)).
                    ForMember(dest => dest.Price, map => map.MapFrom(src => src.Price)).
                    ForMember(dest => dest.MinAge, map => map.MapFrom(src => src.RecommendedMinAge)).
                    ForMember(dest => dest.MaxAge, map => map.MapFrom(src => src.RecommendedMaxAge)).
                    ForMember(dest => dest.IsIndoor, map => map.MapFrom(src => src.Indoor)).
                    ForMember(dest => dest.Recommendation, map => map.MapFrom(src => src.Recommendation));
            });
            return config.CreateMapper();
        }
    }
}
