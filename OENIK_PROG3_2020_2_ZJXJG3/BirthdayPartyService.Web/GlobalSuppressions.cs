﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "yolo", Scope = "member", Target = "~P:BirthdayPartyService.Web.Models.BirthdayServiceViewModel.ListOfServices")]
[assembly: SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "ok", Scope = "type", Target = "~T:BirthdayPartyService.Web.Program")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Pending>", Scope = "member", Target = "~P:BirthdayPartyService.Web.Models.BirthdayServiceViewModel.ListOfServices")]
[assembly: SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "<Pending>", Scope = "type", Target = "~T:BirthdayPartyService.Web.Models.MapperFactory")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:BirthdayPartyService.Web.Controllers.BirthdayServicesController.#ctor(BirthdayPartyService.Logic.ICompanyLogic,BirthdayPartyService.Logic.IVisibleContentLogic,AutoMapper.IMapper)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "b", Scope = "member", Target = "~M:BirthdayPartyService.Web.Controllers.BirthdayServicesApiController.ModOneService(BirthdayPartyService.Web.Models.BirthdayService)~BirthdayPartyService.Web.Controllers.BirthdayServicesApiController.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "o", Scope = "member", Target = "~M:BirthdayPartyService.Web.Controllers.BirthdayServicesApiController.AddOneService(BirthdayPartyService.Web.Models.BirthdayService)~BirthdayPartyService.Web.Controllers.BirthdayServicesApiController.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "g", Scope = "member", Target = "~M:BirthdayPartyService.Web.Controllers.BirthdayServicesApiController.AddOneService(BirthdayPartyService.Web.Models.BirthdayService)~BirthdayPartyService.Web.Controllers.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "i", Scope = "member", Target = "~M:BirthdayPartyService.Web.Controllers.BirthdayServicesApiController.ModOneService(BirthdayPartyService.Web.Models.BirthdayService)~BirthdayPartyService.Web.Controllers.ApiResult")]
