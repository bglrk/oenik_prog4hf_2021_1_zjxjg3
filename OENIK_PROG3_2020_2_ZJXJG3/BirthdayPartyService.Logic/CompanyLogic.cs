﻿// <copyright file="CompanyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BirthdayPartyService.Data.Models;
    using BirthdayPartyService.Repo;

    /// <summary>
    /// This is the company logic class.
    /// </summary>
    public class CompanyLogic : ICompanyLogic
    {
        /// <summary>
        /// This is the order repo.
        /// </summary>
        private IOrdersRepository orderRepo;

        /// <summary>
        /// This is the customer repo.
        /// </summary>
        private ICustomersRepository customerRepo;

        /// <summary>
        /// This is the clown repo.
        /// </summary>
        private IClownsRepository clownRepo;

        /// <summary>
        /// This is the service repo.
        /// </summary>
        private IServicesRepository serviceRepo;

        /// <summary>
        /// This is the connector repo.
        /// </summary>
        private IConnectorRepository connectorRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyLogic"/> class.
        /// </summary>
        /// <param name="orderRepo">IOrdersRepository type parameter.</param>
        /// <param name="customerRepo">ICustomerRepository type parameter.</param>
        /// <param name="clownRepo">IClownsRepository type parameter.</param>
        /// <param name="serviceRepo">IServicesRepository type parameter.</param>
        /// <param name="connectorRepo">IConnectorRepository type parameter.</param>
        public CompanyLogic(IOrdersRepository orderRepo, ICustomersRepository customerRepo, IClownsRepository clownRepo, IServicesRepository serviceRepo, IConnectorRepository connectorRepo)
        {
            this.orderRepo = orderRepo;
            this.customerRepo = customerRepo;
            this.clownRepo = clownRepo;
            this.serviceRepo = serviceRepo;
            this.connectorRepo = connectorRepo;
        }

        /// <summary>
        /// This method can change the price of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newPrice">This is the new price of the clown. </param>
        public void ChangeClownPrice(int id, int newPrice)
        {
            this.clownRepo.ChangePrice(id, newPrice);
        }

        /// <summary>
        /// This method will change the recommendation rating of a service.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="rating">New rating for a service.</param>
        public void ChangeRecommendation(int id, int rating)
        {
            ServiceOptions serv = this.serviceRepo.GetOne(id);
            if (serv == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }
            else if (rating > 10 || rating < 1)
            {
                throw new FormatException("ERROR: Value must be between 1 and 10!");
            }
            else
            {
                this.serviceRepo.UpdateRecommendation(id, rating);
            }
        }

        /// <summary>
        /// This method will change the price of a service.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newPrice">This is the new price of the service. </param>
        public void ChangeServicePrice(int id, int newPrice)
        {
            this.serviceRepo.ChangePrice(id, newPrice);
        }

        /// <summary>
        /// This method will change the stage name of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newName">This is the new stage name of the clown.</param>
        public void ChangeStageName(int id, string newName)
        {
            this.clownRepo.ChangeStageName(id, newName);
        }

        /// <summary>
        /// This method will change the working hours of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newhours">The new working hours of the clown.</param>
        public void ChangeWorkingHours(int id, int newhours)
        {
            this.clownRepo.ChangeWorkingHours(id, newhours);
        }

        /// <summary>
        /// This method will edit a service.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <param name="newService">The parameter is the new service.</param>
        public void ChangeServiceFull(int id, ServiceOptions newService)
        {
            if (newService != null)
            {
                this.serviceRepo.UpdateServiceFull(id, newService);
            }
        }

        /// <summary>
        /// This method will edit a service.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <param name="name">The parameter is a name.</param>
        /// <param name="price">The parameter is a price.</param>
        /// <param name="min">The parameter is the min. recommended age.</param>
        /// <param name="max">The parameter is the max. recommended age.</param>
        /// <param name="recc">The parameter is the reccomendation of the service.</param>
        /// <param name="indoor">The paramter is a boolean.</param>
        /// <returns>Returns true if success.</returns>
        public bool ChangeServiceFullBool2(int id, string name, int price, int min, int max, int recc, bool indoor)
        {
            ServiceOptions newService = new ServiceOptions();
            newService.Id = id;
            newService.Name = name;
            newService.Price = price;
            newService.RecommendedMinAge = min;
            newService.RecommendedMaxAge = max;
            newService.Recommendation = recc;
            newService.Indoor = indoor;

            if (newService != null)
            {
                this.serviceRepo.UpdateServiceFull(id, newService);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This method will edit a service.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <param name="newService">The parameter is the new service.</param>
        /// <returns>Returns true if success.</returns>
        public bool ChangeServiceFullBool(int id, ServiceOptions newService)
        {
            if (newService != null)
            {
                this.serviceRepo.UpdateServiceFull(id, newService);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This method gets every Customers records.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        public IList<Customers> GetAllCustomers()
        {
            return this.customerRepo.GetAll().ToList();
        }

        /// <summary>
        /// This method gets every Orders records.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        public IList<Orders> GetAllOrders()
        {
            return this.orderRepo.GetAll().ToList();
        }

        /// <summary>
        /// This method gets every ConnectorOrdersServices records.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        public IList<ConnectorOrdersServices> GetAllConnections()
        {
            return this.connectorRepo.GetAll().ToList();
        }

        /// <summary>
        /// This method gets one ConnectorOrdersServices record.
        /// </summary>
        /// <param name="id">Parameter named id.</param>
        /// <returns>Returns a ConnectorOrdersServices object.</returns>
        public ConnectorOrdersServices GetOneConnection(int id)
        {
            ConnectorOrdersServices conn = this.connectorRepo.GetOne(id);
            if (conn == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }

            return this.connectorRepo.GetOne(id);
        }

        /// <summary>
        /// This method gets one customer record.
        /// </summary>
        /// <param name="id">Parameter named id.</param>
        /// <returns>Returns a Customer object.</returns>
        public Customers GetOneCustomer(int id)
        {
            Customers cust = this.customerRepo.GetOne(id);
            if (cust == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }

            return this.customerRepo.GetOne(id);
        }

        /// <summary>
        /// This method gets one order record.
        /// </summary>
        /// <param name="id">Parameter named id.</param>
        /// <returns>Returns an Orders object.</returns>
        public Orders GetOneOrder(int id)
        {
            Orders ord = this.orderRepo.GetOne(id);
            if (ord == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }

            return this.orderRepo.GetOne(id);
        }

        /// <summary>
        /// This method inserts a new Clown object into the database.
        /// </summary>
        /// <param name="name">The parameter is a string.</param>
        /// <param name="stagename">The parameter is string. </param>
        /// <param name="gender">The parameter is a string. Must be only one char.</param>
        /// <param name="duration">The parameter is an integer.</param>
        /// <param name="price">The parameter is a number.</param>
        /// <returns>Returns a clown object.</returns>
        public Clowns InsertNewClown(string name, string stagename, string gender, int duration, int price)
        {
            Clowns newObj = new Clowns()
            {
                Name = name,
                StageName = stagename,
                Gender = gender,
                Duration = duration,
                Price = price,
            };
            this.clownRepo.Insert(newObj);
            return newObj;
        }

        /// <summary>
        /// This method removes a Clown object from the database by id.
        /// </summary>
        /// <param name="id">The parameter is an int.</param>
        public void DeleteClown(int id)
        {
            Clowns clown = this.clownRepo.GetOne(id);
            if (clown == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }
            else
            {
                this.clownRepo.Remove(id);
            }
        }

        /// <summary>
        /// This method inserts a new ServiceOptions object into the database.
        /// </summary>
        /// <param name="name">The parameter is a string.</param>
        /// <param name="price">Price as an int.</param>
        /// <param name="min">Minimum recommended age.</param>
        /// <param name="max">Maximum recommended age.</param>
        /// <param name="popularity">The popularity of the service.</param>
        /// <param name="indoor">Boolean for the indoor property.</param>
        /// <returns>Returns a ServiceOptions object.</returns>
        public ServiceOptions InsertNewService(string name, int price, int min, int max, int popularity, bool indoor)
        {
            if (popularity > 10 || popularity < 1)
            {
                throw new FormatException("ERROR: Rating must be between 1 and 10!");
            }
            else
            {
                ServiceOptions newObj = new ServiceOptions()
                {
                    Name = name,
                    Price = price,
                    RecommendedMinAge = min,
                    RecommendedMaxAge = max,
                    Recommendation = popularity,
                    Indoor = indoor,
                };
                this.serviceRepo.Insert(newObj);
                return newObj;
            }
        }

        /// <summary>
        /// This method removes a serviceoptions object from the database.
        /// </summary>
        /// <param name="id">Integer.</param>
        public void DeleteService(int id)
        {
            ServiceOptions service = this.serviceRepo.GetOne(id);
            if (service == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }
            else
            {
                this.serviceRepo.Remove(id);
            }
        }

        /// <summary>
        /// This method removes a serviceoptions object from the database.
        /// </summary>
        /// <param name="id">Integer.</param>
        /// <returns>Returns true if success.</returns>
        public bool DeleteServiceBool(int id)
        {
            ServiceOptions service = this.serviceRepo.GetOne(id);
            if (service == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }
            else
            {
                this.serviceRepo.Remove(id);
                return true;
            }
        }

        /// <summary>
        /// This is a LINQ query to show how much money the customers spent.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        public IList<OrderCostsResults> OrderCosts()
        {
            IList<Orders> orderList = this.orderRepo.GetAll().ToList();
            IList<Clowns> clownList = this.clownRepo.GetAll().ToList();
            IList<ServiceOptions> serviceList = this.serviceRepo.GetAll().ToList();
            IList<ConnectorOrdersServices> connectorList = this.connectorRepo.GetAll().ToList();
            IList<Customers> customerList = this.customerRepo.GetAll().ToList();

            var result = from customer in customerList
                         join orders in from order in from order in from orders in orderList
                                                                    join services in from conn in connectorList
                                                                                     join serv in serviceList
                                                                                     on conn.ServiceId equals serv.Id
                                                                                     select new
                                                                                     {
                                                                                         ConnectorID = conn.Id,
                                                                                         OrderID = conn.OrderId,
                                                                                         ServicePrice = serv.Price,
                                                                                     }
                                                                    on orders.Id equals services.OrderID
                                                                    group services by services.OrderID into order
                                                                    select new
                                                                    {
                                                                        OrderID = order.Key,
                                                                        TotalPrice = order.Sum(x => x.ServicePrice),
                                                                    }
                                                      join cust in orderList
                                                      on order.OrderID equals cust.Id
                                                      select new
                                                      {
                                                          CustomerID = cust.CustomerId,
                                                          order.OrderID,
                                                          order.TotalPrice,
                                                      }
                                        join clown in from order in orderList
                                                      join clown in clownList
                                                      on order.ClownId equals clown.Id
                                                      select new
                                                      {
                                                          OrderID = order.Id,
                                                          ClownPrice = clown.Price,
                                                      }
                                        on order.OrderID equals clown.OrderID
                                        select new
                                        {
                                            order.OrderID,
                                            order.CustomerID,
                                            TotalPrice = order.TotalPrice + clown.ClownPrice,
                                        }
                         on customer.Id equals orders.CustomerID
                         group orders by orders.CustomerID.Value into prices
                         let total = prices.Sum(x => x.TotalPrice)
                         orderby total descending
                         select new OrderCostsResults()
                         {
                             CustomerID = prices.Key,
                             CustomerName = this.customerRepo.GetOne(prices.Key).Name,
                             TotalPrice = total,
                         };

            return result.ToList();
        }

        /// <summary>
        /// This is a LINQ query to show how much money the clown earned and how many jobs they had.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        public IList<ClownSalaryResults> ClownEarnings()
        {
            IList<Clowns> clownsList = this.clownRepo.GetAll().ToList();
            IList<Orders> ordersList = this.orderRepo.GetAll().ToList();

            var result = from clowns in clownsList
                         join orders in ordersList
                         on clowns.Id equals orders.ClownId
                                 group orders by orders.ClownId.Value into grp
                                 let counting = grp.Count()
                                 select new ClownSalaryResults()
                                 {
                                     ClownID = grp.Key,
                                     ClownName = this.clownRepo.GetOne(grp.Key).StageName,
                                     GigsNum = counting,
                                     TotalEarnings = counting * this.clownRepo.GetOne(grp.Key).Price,
                                 };

            return result.ToList();
        }

        /// <summary>
        /// This is a LINQ query to show which customers did not pay attention to the recommended age gap.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        public IList<AgeGapResults> WhoDidNotCare()
        {
            IList<Orders> orderList = this.orderRepo.GetAll().ToList();
            IList<ServiceOptions> serviceList = this.serviceRepo.GetAll().ToList();
            IList<ConnectorOrdersServices> connectorList = this.connectorRepo.GetAll().ToList();
            IList<Customers> customerList = this.customerRepo.GetAll().ToList();

            var result = from data in from records in from records in from orders in orderList
                                                                      join customers in from customers in customerList
                                                                                        join orders in orderList
                                                                                        on customers.Id equals orders.CustomerId
                                                                                        select new
                                                                                        {
                                                                                            CustomerID = customers.Id,
                                                                                            Age = customers.KidAge,
                                                                                        }
                                                                      on orders.CustomerId equals customers.CustomerID
                                                                      let id = customers.CustomerID
                                                                      orderby id
                                                                      select new
                                                                      {
                                                                          customers.CustomerID,
                                                                          customers.Age,
                                                                          OrderID = orders.Id,
                                                                      }
                                                      group records by new { records.OrderID }
                                                          into grp
                                                      select grp.FirstOrDefault()
                                      join orders in from services in serviceList
                                                     join connector in connectorList
                                                     on services.Id equals connector.ServiceId
                                                     let id = connector.OrderId
                                                     orderby id
                                                     select new
                                                     {
                                                         OrderID = id,
                                                         ConnectorID = connector.Id,
                                                         ServiceID = services.Id,
                                                         services.RecommendedMinAge,
                                                         services.RecommendedMaxAge,
                                                     }
                                      on records.OrderID equals orders.OrderID
                                      let id = records.CustomerID
                                      orderby id
                                      select new
                                      {
                                          records.CustomerID,
                                          records.Age,
                                          orders.OrderID,
                                          orders.ServiceID,
                                          orders.RecommendedMinAge,
                                          orders.RecommendedMaxAge,
                                      }
                         where data.Age < data.RecommendedMinAge || data.Age > data.RecommendedMaxAge
                         select new AgeGapResults()
                         {
                             CustomerID = data.CustomerID,
                             CustomerName = this.customerRepo.GetOne(data.CustomerID).Name,
                             KidAge = data.Age,
                             ChosenService = this.serviceRepo.GetOne(data.ServiceID).Name,
                             RecommendedMinAge = data.RecommendedMinAge,
                             RecommendedMaxAge = data.RecommendedMaxAge,
                         };

            return result.ToList();
        }

        /// <summary>
        /// This is the async version of the OrderCosts method.
        /// </summary>
        /// <returns>It returns a Task.</returns>
        public Task<IList<OrderCostsResults>> OrderCostsAsync()
        {
            return Task.Run(this.OrderCosts);
        }

        /// <summary>
        /// This is the async version of the ClownEarnings method.
        /// </summary>
        /// <returns>It returns a Task.</returns>
        public Task<IList<ClownSalaryResults>> ClownEarningsAsync()
        {
            return Task.Run(this.ClownEarnings);
        }

        /// <summary>
        /// This is the async version of the WhoDidNotCareAsync method.
        /// </summary>
        /// <returns>It returns a Task.</returns>
        public Task<IList<AgeGapResults>> WhoDidNotCareAsync()
        {
            return Task.Run(this.WhoDidNotCare);
        }
    }
}
