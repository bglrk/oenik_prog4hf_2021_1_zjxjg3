﻿// <copyright file="ICompanyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using BirthdayPartyService.Data.Models;

    /// <summary>
    /// This is the company interface.
    /// </summary>
    public interface ICompanyLogic
    {
        /// <summary>
        /// This method gets an order by the id.
        /// </summary>
        /// <param name="id">This is the parameter named id.</param>
        /// <returns>It returns a single Orders record. </returns>
        Orders GetOneOrder(int id);

        /// <summary>
        /// This method will list every Orders record.
        /// </summary>
        /// <returns>It returns an IList. </returns>
        IList<Orders> GetAllOrders();

        /// <summary>
        /// This method gets a customer by the id.
        /// </summary>
        /// <param name="id">This is the parameter named id.</param>
        /// <returns>It returns a single customers record.</returns>
        Customers GetOneCustomer(int id);

        /// <summary>
        /// This method will list every Customers record.
        /// </summary>
        /// <returns>It returns an IList.</returns>
        IList<Customers> GetAllCustomers();

        /// <summary>
        /// This method will change the price of a service.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newPrice">This is the new price of the service. </param>
        void ChangeServicePrice(int id, int newPrice);

        /// <summary>
        /// This method can change the price of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newPrice">This is the new price of the clown. </param>
        void ChangeClownPrice(int id, int newPrice);

        /// <summary>
        /// This method will change the stage name of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newName">This is the new stage name of the clown.</param>
        void ChangeStageName(int id, string newName);

        /// <summary>
        /// This method will change the working hours of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newhours">The new working hours of the clown.</param>
        void ChangeWorkingHours(int id, int newhours);

        /// <summary>
        /// This method will change the recommendation rating of a service.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="rating">New rating for a service.</param>
        void ChangeRecommendation(int id, int rating);

        /// <summary>
        /// This method removes a serviceoptions object from the database.
        /// </summary>
        /// <param name="id">Integer.</param>
        /// <returns>Returns true if success.</returns>
        bool DeleteServiceBool(int id);

        /// <summary>
        /// This method will edit a service.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <param name="newService">The parameter is the new service.</param>
        /// <returns>Returns true if success.</returns>
        bool ChangeServiceFullBool(int id, ServiceOptions newService);

        /// <summary>
        /// This method will edit a service.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <param name="name">The parameter is a name.</param>
        /// <param name="price">The parameter is a price.</param>
        /// <param name="min">The parameter is the min. recommended age.</param>
        /// <param name="max">The parameter is the max. recommended age.</param>
        /// <param name="recc">The parameter is the reccomendation of the service.</param>
        /// <param name="indoor">The paramter is a boolean.</param>
        /// <returns>Returns true if success.</returns>
        bool ChangeServiceFullBool2(int id, string name, int price, int min, int max, int recc, bool indoor);

        /// <summary>
        /// This method inserts a new ServiceOptions object into the database.
        /// </summary>
        /// <param name="name">The parameter is a string.</param>
        /// <param name="price">Price as an int.</param>
        /// <param name="min">Minimum recommended age.</param>
        /// <param name="max">Maximum recommended age.</param>
        /// <param name="popularity">The popularity of the service.</param>
        /// <param name="indoor">Boolean for the indoor property.</param>
        /// <returns>Returns a ServiceOptions object.</returns>
        ServiceOptions InsertNewService(string name, int price, int min, int max, int popularity, bool indoor);

        /// <summary>
        /// This method will edit a service.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <param name="newService">The parameter is the new service.</param>
        void ChangeServiceFull(int id, ServiceOptions newService);

        /// <summary>
        /// This method is a NON Crud query, it shows the total money the customer spent.
        /// </summary>
        /// <returns>It returns an IList.</returns>
        IList<OrderCostsResults> OrderCosts();

        /// <summary>
        /// This is a LINQ query to show how much money the clown earned and how many jobs they had.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        IList<ClownSalaryResults> ClownEarnings();

        /// <summary>
        /// This is a LINQ query to show which customers did not pay attention to the recommended age gap.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        IList<AgeGapResults> WhoDidNotCare();
    }
}
