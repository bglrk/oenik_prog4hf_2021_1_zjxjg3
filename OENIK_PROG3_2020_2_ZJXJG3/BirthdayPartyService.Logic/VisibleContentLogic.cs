﻿// <copyright file="VisibleContentLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BirthdayPartyService.Data.Models;
    using BirthdayPartyService.Repo;

    /// <summary>
    /// This is the visible content logic class.
    /// </summary>
    public class VisibleContentLogic : IVisibleContentLogic
    {
        /// <summary>
        /// This is the clown repo.
        /// </summary>
        private IClownsRepository clownRepo;

        /// <summary>
        /// This is the service repo.
        /// </summary>
        private IServicesRepository serviceRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisibleContentLogic"/> class.
        /// </summary>
        /// <param name="clownRepo">IClownsRepository type parameter.</param>
        /// <param name="serviceRepo">IServicesRepository type parameter.</param>
        public VisibleContentLogic(IClownsRepository clownRepo, IServicesRepository serviceRepo)
        {
            this.clownRepo = clownRepo;
            this.serviceRepo = serviceRepo;
        }

        /// <summary>
        /// This method gets every Clowns records.
        /// </summary>
        /// <returns>Returns an IList. </returns>
        public IList<Clowns> GetAllClowns()
        {
            return this.clownRepo.GetAll().ToList();
        }

        /// <summary>
        /// This method gets every Servicesx records.
        /// </summary>
        /// <returns>Returns an IList.</returns>
        public IList<ServiceOptions> GetAllServices()
        {
            return this.serviceRepo.GetAll().ToList();
        }

        /// <summary>
        /// This method gets one clown record.
        /// </summary>
        /// <param name="id">Parameter named id.</param>
        /// <returns>Returns a Clown object.</returns>
        public Clowns GetOneClown(int id)
        {
            Clowns clown = this.clownRepo.GetOne(id);
            if (clown == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }

            return this.clownRepo.GetOne(id);
        }

        /// <summary>
        /// This method gets one service record.
        /// </summary>
        /// <param name="id">Parameter named id.</param>
        /// <returns>Returns a ServiceOptions object.</returns>
        public ServiceOptions GetOneService(int id)
        {
            ServiceOptions service = this.serviceRepo.GetOne(id);
            if (service == null)
            {
                throw new InvalidOperationException("ERROR: No corresponding record! Did you mistype the ID?");
            }

            return this.serviceRepo.GetOne(id);
        }
    }
}
