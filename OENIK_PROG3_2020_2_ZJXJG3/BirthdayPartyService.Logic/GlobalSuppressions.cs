﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "silly thing.", Scope = "member", Target = "~M:BirthdayPartyService.Logic.ClownSalaryResults.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "silly thing.", Scope = "member", Target = "~M:BirthdayPartyService.Logic.AgeGapResults.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "silly thing.", Scope = "member", Target = "~M:BirthdayPartyService.Logic.OrderCostsResults.GetHashCode~System.Int32")]
[assembly: SuppressMessage("Maintainability", "CA1508:Avoid dead conditional code", Justification = "jolvan", Scope = "member", Target = "~M:BirthdayPartyService.Logic.CompanyLogic.ChangeServiceFullBool2(System.Int32,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)~System.Boolean")]
