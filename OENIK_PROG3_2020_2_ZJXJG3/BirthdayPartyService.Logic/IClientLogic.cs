﻿// <copyright file="IClientLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// This is the client interface.
    /// </summary>
    public interface IClientLogic
    {
        /// <summary>
        /// This method can change the datetime for an order.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newDate">This is the new date time. </param>
        void ChangeOrderDate(int id, DateTime newDate);

        /// <summary>
        /// This method will change the number of participants.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newNum">This is the number of kids. </param>
        void ChangeNumOfKids(int id, int newNum);

        /// <summary>
        /// This method will change the customer's address.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="address">New address.</param>
        /// <param name="city">New city.</param>
        void ChangeCustomerAddress(int id, string address, string city);

        /// <summary>
        /// This method will change the customer's email address.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="email">New email address. </param>
        void ChangeCustomerEmail(int id, string email);
    }
}
