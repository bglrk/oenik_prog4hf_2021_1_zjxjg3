﻿// <copyright file="ClownSalaryResults.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// This is the class for my third non-crud method.
    /// </summary>
    public class ClownSalaryResults
    {
        /// <summary>
        /// Gets or sets the clown's ID.
        /// </summary>
        public int ClownID { get; set; }

        /// <summary>
        /// Gets or sets the clown's name.
        /// </summary>
        public string ClownName { get; set; }

        /// <summary>
        /// Gets or sets the number of jobs the clown had.
        /// </summary>
        public int GigsNum { get; set; }

        /// <summary>
        /// Gets or sets the total earnings of the clown.
        /// </summary>
        public int TotalEarnings { get; set; }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>Returns a formatted string.</returns>
        public override string ToString()
        {
            return $"Clown's ID: {this.ClownID}, Clown name: {this.ClownName}, Number of jobs: {this.GigsNum}, Total earning: {this.TotalEarnings}.";
        }

        /// <summary>
        /// This is the overriden version of the Equals method.
        /// </summary>
        /// <param name="obj">The parameter is an object.</param>
        /// <returns>Returns false or true.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ClownSalaryResults)
            {
                ClownSalaryResults other = obj as ClownSalaryResults;
                return this.ClownID == other.ClownID &&
                    this.ClownName == other.ClownName &&
                    this.GigsNum == other.GigsNum &&
                    this.TotalEarnings == other.TotalEarnings;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This is the overriden version of the GetHashCode method.
        /// </summary>
        /// <returns>Returns an integer.</returns>
        public override int GetHashCode()
        {
            return this.ClownID.GetHashCode() + this.ClownName.GetHashCode();
        }
    }
}
