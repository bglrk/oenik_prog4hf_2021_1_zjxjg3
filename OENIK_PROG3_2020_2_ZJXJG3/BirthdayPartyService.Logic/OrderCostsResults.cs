﻿// <copyright file="OrderCostsResults.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// This is the class for my first non-crud method.
    /// </summary>
    public class OrderCostsResults
    {
        /// <summary>
        /// Gets or setsthe customer ID.
        /// </summary>
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets the customer's name.
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the price of the order.
        /// </summary>
        public int TotalPrice { get; set; }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>Returns a nicely formatted string.</returns>
        public override string ToString()
        {
            return $"Customer's ID = {this.CustomerID}, Customer's name = {this.CustomerName}, Order's cost: {this.TotalPrice} USD.";
        }

        /// <summary>
        /// This is the overriden version of the Equals method.
        /// </summary>
        /// <param name="obj">The parameter is an object.</param>
        /// <returns>Returns false or true.</returns>
        public override bool Equals(object obj)
        {
            if (obj is OrderCostsResults)
            {
                OrderCostsResults other = obj as OrderCostsResults;
                return this.CustomerID == other.CustomerID &&
                    this.CustomerName == other.CustomerName &&
                    this.TotalPrice == other.TotalPrice;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This is the overriden version of the GetHashCode method.
        /// </summary>
        /// <returns>Returns an integer.</returns>
        public override int GetHashCode()
        {
            return this.CustomerID.GetHashCode() + this.CustomerName.GetHashCode();
        }
    }
}
