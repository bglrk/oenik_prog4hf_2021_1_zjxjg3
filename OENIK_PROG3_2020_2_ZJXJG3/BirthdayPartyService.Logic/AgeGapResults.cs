﻿// <copyright file="AgeGapResults.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// This is the class for my second non-crud method.
    /// </summary>
    public class AgeGapResults
    {
        /// <summary>
        /// Gets or sets the customer's ID.
        /// </summary>
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets the customer's name.
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the customer's kid's age.
        /// </summary>
        public int KidAge { get; set; }

        /// <summary>
        /// Gets or sets the chosen service.
        /// </summary>
        public string ChosenService { get; set; }

        /// <summary>
        /// Gets or sets the minimum age for the service.
        /// </summary>
        public int RecommendedMinAge { get; set; }

        /// <summary>
        /// Gets or sets the maximum age for the service.
        /// </summary>
        public int RecommendedMaxAge { get; set; }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>Returns a nicely formatted string.</returns>
        public override string ToString()
        {
            return $"Customer's ID: {this.CustomerID}, kid's age: {this.KidAge}, Chosen service: {this.ChosenService}, Min.Age: {this.RecommendedMinAge}, Max.Age: {this.RecommendedMaxAge}.";
        }

        /// <summary>
        /// This is the overriden version of the Equals method.
        /// </summary>
        /// <param name="obj">The parameter is an object.</param>
        /// <returns>Returns false or true.</returns>
        public override bool Equals(object obj)
        {
            if (obj is AgeGapResults)
            {
                AgeGapResults other = obj as AgeGapResults;
                return this.CustomerID == other.CustomerID &&
                    this.CustomerName == other.CustomerName &&
                    this.KidAge == other.KidAge &&
                    this.ChosenService == other.ChosenService &&
                    this.RecommendedMinAge == other.RecommendedMinAge &&
                    this.RecommendedMaxAge == other.RecommendedMaxAge;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This is the overriden version of the GetHashCode method.
        /// </summary>
        /// <returns>Returns an integer.</returns>
        public override int GetHashCode()
        {
            return this.CustomerID.GetHashCode() + this.CustomerName.GetHashCode();
        }
    }
}
