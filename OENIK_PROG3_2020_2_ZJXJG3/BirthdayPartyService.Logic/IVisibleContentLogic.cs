﻿// <copyright file="IVisibleContentLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BirthdayPartyService.Data.Models;

    /// <summary>
    /// This is the interface for the visible content logic class.
    /// </summary>
    public interface IVisibleContentLogic
    {
        /// <summary>
        /// This method gets a service by the id.
        /// </summary>
        /// <param name="id">This is the parameter named id.</param>
        /// <returns>It returns a single Servicesx record.</returns>
        ServiceOptions GetOneService(int id);

        /// <summary>
        /// This method will list every Services record.
        /// </summary>
        /// <returns>It returns an IList.</returns>
        IList<ServiceOptions> GetAllServices();

        /// <summary>
        /// This method gets a clown by the id.
        /// </summary>
        /// <param name="id">This is the parameter named id.</param>
        /// <returns>It returns a single Clowns record.</returns>
        Clowns GetOneClown(int id);

        /// <summary>
        /// This method will list every Clowns record.
        /// </summary>
        /// <returns>It returns an IList.</returns>
        IList<Clowns> GetAllClowns();
    }
}
