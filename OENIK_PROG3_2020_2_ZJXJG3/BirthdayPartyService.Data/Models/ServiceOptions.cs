﻿// <copyright file="ServiceOptions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Services table which represents the services that can be chosen for a birthday party.
    /// </summary>
    [Table("services")]
    public class ServiceOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceOptions"/> class.
        /// </summary>
        public ServiceOptions()
        {
            this.ConnectorOrdersServices = new HashSet<ConnectorOrdersServices>();
        }

        /// <summary>
        /// Gets or sets the primary key of the service object.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the service object.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the price for the service.
        /// </summary>
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets the minimum recommended age for the service.
        /// </summary>
        [Required]
        public int RecommendedMinAge { get; set; }

        /// <summary>
        /// Gets or sets the maximum recommended age for the service.
        /// </summary>
        [Required]
        public int RecommendedMaxAge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the service can be held indoor or not.
        /// </summary>
        [Required]
        public bool Indoor { get; set; }

        /// <summary>
        /// Gets or sets the value of the recommendation. Number must be between 1 and 10.
        /// </summary>
        [Range(1, 10)]
        public int Recommendation { get; set; }

        /// <summary>
        /// Gets the ConnectorOrdersServices collection.
        /// </summary>
        [NotMapped]
        public virtual ICollection<ConnectorOrdersServices> ConnectorOrdersServices { get; }

        /// <summary>
        /// Writes out data in string format.
        /// </summary>
        /// <returns> Returns a string with all needed information. </returns>
        public override string ToString()
        {
            return $"\n{this.Id,3} | {this.Recommendation,2}/10 {this.Price,7} USD {this.RecommendedMinAge,10} - {this.RecommendedMaxAge} y/o {this.Indoor,10}\t{this.Name,-1}";
        }
    }
}
