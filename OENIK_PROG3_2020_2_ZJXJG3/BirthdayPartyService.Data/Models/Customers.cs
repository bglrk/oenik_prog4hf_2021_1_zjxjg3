﻿// <copyright file="Customers.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Customers table which has data about each customer.
    /// </summary>
    [Table("customers")]
    public class Customers
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customers"/> class.
        /// </summary>
        public Customers()
        {
            this.Orders = new HashSet<Orders>();
        }

        /// <summary>
        /// Gets or sets the primary key of the customer object.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the address of the customer.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the city of the customer.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the email address of the customer.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the age of the kid of the customer.
        /// </summary>
        [Required]
        public int KidAge { get; set; }

        /// <summary>
        /// Gets or sets the number of kids of the customer.
        /// </summary>
        [Required]
        public int NumOfKids { get; set; }

        /// <summary>
        /// Gets the collection of the orders.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Orders> Orders { get; }

        /// <summary>
        /// Writes out data in string format.
        /// </summary>
        /// <returns> Returns a string with all needed information. </returns>
        public override string ToString()
        {
            return $"\n{this.Id,3} | {this.Name,-20} {this.Email,-28} {this.KidAge,5} {this.NumOfKids,6} {this.Orders.Count,7} \t{this.Address}, {this.City}";
        }
    }
}
