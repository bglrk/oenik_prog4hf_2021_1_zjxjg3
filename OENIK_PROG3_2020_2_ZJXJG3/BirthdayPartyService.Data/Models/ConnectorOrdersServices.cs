﻿// <copyright file="ConnectorOrdersServices.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Connector table for the many-to-many relationship of the orders and services tables.
    /// </summary>
    [Table("ConnectorOrdersServices")]
    public class ConnectorOrdersServices
    {
        /// <summary>
        /// Gets or sets the ID for the connector table.
        /// </summary>
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the key of the order object.
        /// </summary>
        [ForeignKey(nameof(Orders))]
        public int? OrderId { get; set; }

        /// <summary>
        /// Gets or sets the order object.
        /// </summary>
        public virtual Orders Orders { get; set; }

        /// <summary>
        /// Gets or sets the key of the service object.
        /// </summary>
        [ForeignKey(nameof(Services))]
        public int? ServiceId { get; set; }

        /// <summary>
        /// Gets or sets the service object.
        /// </summary>
        public virtual ServiceOptions Services { get; set; }

        /// <summary>
        /// Writes out data in string format.
        /// </summary>
        /// <returns> Returns a string with all needed information. </returns>
        public override string ToString()
        {
            return $"{this.Id,3} | {this.Orders?.Id,5}\t {this.Services?.Name ?? "N/A",-40} {this.Services?.Price ?? 0,10} USD";
        }
    }
}
