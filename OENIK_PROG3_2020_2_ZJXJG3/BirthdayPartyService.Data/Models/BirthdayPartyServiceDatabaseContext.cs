﻿// <copyright file="BirthdayPartyServiceDatabaseContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// DbContext class which ensures the database is created, creates the connections and adds the records to the database.
    /// </summary>
    public class BirthdayPartyServiceDatabaseContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BirthdayPartyServiceDatabaseContext"/> class.
        /// </summary>
        public BirthdayPartyServiceDatabaseContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BirthdayPartyServiceDatabaseContext"/> class. It has one parameter.
        /// </summary>
        /// <param name="options"> Uses parameter named options. </param>
        public BirthdayPartyServiceDatabaseContext(DbContextOptions<BirthdayPartyServiceDatabaseContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets the Customers DbSet.
        /// </summary>
        public virtual DbSet<Customers> Customers { get; set; }

        /// <summary>
        /// Gets or sets the Clowns DbSet.
        /// </summary>
        public virtual DbSet<Clowns> Clowns { get; set; }

        /// <summary>
        /// Gets or sets the Services DbSet.
        /// </summary>
        public virtual DbSet<ServiceOptions> Services { get; set; }

        /// <summary>
        /// Gets or sets the Orders DbSet.
        /// </summary>
        public virtual DbSet<Orders> Orders { get; set; }

        /// <summary>
        /// Gets or sets the Connector DbSet.
        /// </summary>
        public virtual DbSet<ConnectorOrdersServices> ConnectorTable { get; set; }

        /// <summary>
        /// Overrides the OnConfiguring method and ensures lazyloading.
        /// </summary>
        /// <param name="optionsBuilder"> Uses parameter named optionBuilder. </param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder.
                    UseLazyLoadingProxies().
                    UseSqlServer(@"data source=(LocalDB)\MSSQLLocalDB; Attachdbfilename=|DataDirectory|\BirthdayPartyServiceDatabase.mdf; Integrated security=True; MultipleActiveResultSets=True");
            }
        }

        /// <summary>
        /// Overrides the onModelCreating method, creates the records and the relationship between the tables.
        /// </summary>
        /// <param name="modelBuilder"> Uses parameter named modelBuilder. </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
                ServiceOptions service1 = new ServiceOptions() { Id = 1, Name = "Super funny clown Show", Price = 100, RecommendedMinAge = 5, RecommendedMaxAge = 18, Indoor = true, Recommendation = 10 };
                ServiceOptions service2 = new ServiceOptions() { Id = 2, Name = "Face painting, glitter tattooing", Price = 500, RecommendedMinAge = 5, RecommendedMaxAge = 18, Indoor = true, Recommendation = 8 };
                ServiceOptions service3 = new ServiceOptions() { Id = 3, Name = "Inflatable castle", Price = 320, RecommendedMinAge = 5, RecommendedMaxAge = 12, Indoor = false, Recommendation = 5 };
                ServiceOptions service4 = new ServiceOptions() { Id = 4, Name = "Pony horse riding", Price = 280, RecommendedMinAge = 7, RecommendedMaxAge = 18, Indoor = false, Recommendation = 6 };
                ServiceOptions service5 = new ServiceOptions() { Id = 5, Name = "Stand-up comedy", Price = 150, RecommendedMinAge = 13, RecommendedMaxAge = 18, Indoor = true, Recommendation = 7 };
                ServiceOptions service6 = new ServiceOptions() { Id = 6, Name = "Magic show", Price = 100, RecommendedMinAge = 5, RecommendedMaxAge = 18, Indoor = true, Recommendation = 10 };

                Clowns clown1 = new Clowns() { Id = 1, Name = "Greg Arnold", StageName = "Freckles", Gender = "M", Duration = 4, Price = 130 };
                Clowns clown2 = new Clowns() { Id = 2, Name = "Maeve Kouma", StageName = "Luna", Gender = "F", Duration = 4, Price = 100 };
                Clowns clown3 = new Clowns() { Id = 3, Name = "Viktor Terrell", StageName = "Clumsy", Gender = "M", Duration = 4, Price = 150 };
                Clowns clown4 = new Clowns() { Id = 4, Name = "Maja Foster", StageName = "Smiley", Gender = "F", Duration = 6, Price = 140 };
                Clowns clown5 = new Clowns() { Id = 5, Name = "Liberty Leal", StageName = "Bonbon", Gender = "N", Duration = 4, Price = 130 };
                Clowns clown6 = new Clowns() { Id = 6, Name = "Greg Arnold", StageName = "Jimbo", Gender = "M", Duration = 5, Price = 150 };

                Customers customer1 = new Customers() { Id = 1, Name = "Nathan Mays", Address = "45 Creekside Ave", City = "Los Angeles", Email = "mays.nathan@fakemail.com", KidAge = 9, NumOfKids = 30 };
                Customers customer2 = new Customers() { Id = 2, Name = "Eliza Nolan", Address = "97 Helen St.", City = "Bellflower", Email = "lizanol@mybusiness.it", KidAge = 16, NumOfKids = 18 };
                Customers customer3 = new Customers() { Id = 3, Name = "Georgiana Farley", Address = "8754 Westminster Ave.", City = "Milpitas", Email = "farlay@email.com", KidAge = 7, NumOfKids = 25 };
                Customers customer4 = new Customers() { Id = 4, Name = "Jodi Mcmanus", Address = "54 Overlook Drive", City = "Moreno Valley", Email = "mcmanusjodi@jodimail.com", KidAge = 12, NumOfKids = 34 };
                Customers customer5 = new Customers() { Id = 5, Name = "Ellie-May Mueller", Address = "499 Queen Drive", City = "Los Angeles", Email = "princessellie@fakemail.com", KidAge = 16, NumOfKids = 29 };
                Customers customer6 = new Customers() { Id = 6, Name = "Matthew Lam", Address = "9701 Edgefield Drive", City = "Montebello", Email = "matthew79@montebello.it", KidAge = 9, NumOfKids = 31 };
                Customers customer7 = new Customers() { Id = 7, Name = "Walter Carrillo", Address = "88 Cedarwood St.", City = "Long Beach", Email = "walterc@freemail.com", KidAge = 6, NumOfKids = 16 };
                Customers customer8 = new Customers() { Id = 8, Name = "Karol Melton", Address = "5 Trusel Rd.", City = "Los Angeles", Email = "karolmarol@kmail.com", KidAge = 14, NumOfKids = 16 };
                Customers customer9 = new Customers() { Id = 9, Name = "Teresa Vasquez", Address = "8 Oak St.", City = "San Francisco", Email = "teresavasquez@maffiamail.com", KidAge = 15, NumOfKids = 16 };
                Customers customer10 = new Customers() { Id = 10, Name = "Lina Villalobos", Address = "257 Courtland Ave.", City = "Los Angeles", Email = "villalobos@linamail.com", KidAge = 13, NumOfKids = 24 };
                Customers customer11 = new Customers() { Id = 11, Name = "Theia Bennett", Address = "978 Gulf Drive", City = "San Diego", Email = "theia@bennett.com", KidAge = 8, NumOfKids = 24 };
                Customers customer12 = new Customers() { Id = 12, Name = "Pharrell Duke", Address = "77 San Carlos Drive", City = "North Hollywood", Email = "dukepharrel9@hollywood.com", KidAge = 9, NumOfKids = 29 };
                Customers customer13 = new Customers() { Id = 13, Name = "Steven Squires", Address = "5 Acacia Street", City = "San Diego", Email = "ssquirres@email.com", KidAge = 7, NumOfKids = 25 };
                Customers customer14 = new Customers() { Id = 14, Name = "Lorelai Flower", Address = "7505 Bridle Ave.", City = "Ontario", Email = "lorelai@flowermail.com", KidAge = 11, NumOfKids = 18 };
                Customers customer15 = new Customers() { Id = 15, Name = "Jayden Logan", Address = "88 Lexington St. Suite 944", City = "Los Angeles", Email = "jaydenlgn@losangelesmail.com", KidAge = 16, NumOfKids = 24 };

                Orders order1 = new Orders() { Id = 1, CustomerId = customer1.Id, ClownId = clown4.Id, DateTime = new DateTime(2020, 06, 19) };
                Orders order2 = new Orders() { Id = 2, CustomerId = customer3.Id, ClownId = clown1.Id, DateTime = new DateTime(2020, 06, 23) };
                Orders order3 = new Orders() { Id = 3, CustomerId = customer5.Id, ClownId = clown2.Id, DateTime = new DateTime(2020, 06, 26) };
                Orders order4 = new Orders() { Id = 4, CustomerId = customer4.Id, ClownId = clown5.Id, DateTime = new DateTime(2020, 07, 01) };
                Orders order5 = new Orders() { Id = 5, CustomerId = customer7.Id, ClownId = clown4.Id, DateTime = new DateTime(2020, 07, 04) };
                Orders order6 = new Orders() { Id = 6, CustomerId = customer6.Id, ClownId = clown5.Id, DateTime = new DateTime(2020, 07, 04) };
                Orders order7 = new Orders() { Id = 7, CustomerId = customer2.Id, ClownId = clown6.Id, DateTime = new DateTime(2020, 07, 08) };
                Orders order8 = new Orders() { Id = 8, CustomerId = customer8.Id, ClownId = clown6.Id, DateTime = new DateTime(2020, 07, 15) };
                Orders order9 = new Orders() { Id = 9, CustomerId = customer13.Id, ClownId = clown6.Id, DateTime = new DateTime(2020, 08, 20) };
                Orders order10 = new Orders() { Id = 10, CustomerId = customer14.Id, ClownId = clown6.Id, DateTime = new DateTime(2020, 08, 23) };
                Orders order11 = new Orders() { Id = 11, CustomerId = customer1.Id, ClownId = clown1.Id, DateTime = new DateTime(2020, 08, 24) };
                Orders order12 = new Orders() { Id = 12, CustomerId = customer9.Id, ClownId = clown5.Id, DateTime = new DateTime(2020, 08, 25) };
                Orders order13 = new Orders() { Id = 13, CustomerId = customer11.Id, ClownId = clown4.Id, DateTime = new DateTime(2020, 08, 30) };
                Orders order14 = new Orders() { Id = 14, CustomerId = customer15.Id, ClownId = clown4.Id, DateTime = new DateTime(2020, 09, 02) };
                Orders order15 = new Orders() { Id = 15, CustomerId = customer8.Id, ClownId = clown3.Id, DateTime = new DateTime(2020, 09, 07) };
                Orders order16 = new Orders() { Id = 16, CustomerId = customer10.Id, ClownId = clown1.Id, DateTime = new DateTime(2020, 09, 18) };
                Orders order17 = new Orders() { Id = 17, CustomerId = customer7.Id, ClownId = clown3.Id, DateTime = new DateTime(2020, 09, 19) };
                Orders order18 = new Orders() { Id = 18, CustomerId = customer12.Id, ClownId = clown3.Id, DateTime = new DateTime(2020, 10, 04) };
                Orders order19 = new Orders() { Id = 19, CustomerId = customer10.Id, ClownId = clown2.Id, DateTime = new DateTime(2020, 10, 27) };

                ConnectorOrdersServices conn1 = new ConnectorOrdersServices() { Id = 1, OrderId = order1.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn2 = new ConnectorOrdersServices() { Id = 2, OrderId = order1.Id, ServiceId = service3.Id };
                ConnectorOrdersServices conn3 = new ConnectorOrdersServices() { Id = 3, OrderId = order1.Id, ServiceId = service2.Id };
                ConnectorOrdersServices conn4 = new ConnectorOrdersServices() { Id = 4, OrderId = order2.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn5 = new ConnectorOrdersServices() { Id = 5, OrderId = order2.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn6 = new ConnectorOrdersServices() { Id = 6, OrderId = order3.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn7 = new ConnectorOrdersServices() { Id = 7, OrderId = order4.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn8 = new ConnectorOrdersServices() { Id = 8, OrderId = order4.Id, ServiceId = service5.Id };
                ConnectorOrdersServices conn9 = new ConnectorOrdersServices() { Id = 9, OrderId = order4.Id, ServiceId = service6.Id };
                ConnectorOrdersServices conn10 = new ConnectorOrdersServices() { Id = 10, OrderId = order4.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn11 = new ConnectorOrdersServices() { Id = 11, OrderId = order5.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn12 = new ConnectorOrdersServices() { Id = 12, OrderId = order5.Id, ServiceId = service2.Id };
                ConnectorOrdersServices conn13 = new ConnectorOrdersServices() { Id = 13, OrderId = order5.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn14 = new ConnectorOrdersServices() { Id = 14, OrderId = order6.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn15 = new ConnectorOrdersServices() { Id = 15, OrderId = order6.Id, ServiceId = service3.Id };
                ConnectorOrdersServices conn16 = new ConnectorOrdersServices() { Id = 16, OrderId = order6.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn17 = new ConnectorOrdersServices() { Id = 17, OrderId = order6.Id, ServiceId = service6.Id };
                ConnectorOrdersServices conn18 = new ConnectorOrdersServices() { Id = 18, OrderId = order7.Id, ServiceId = service6.Id };
                ConnectorOrdersServices conn19 = new ConnectorOrdersServices() { Id = 19, OrderId = order7.Id, ServiceId = service2.Id };
                ConnectorOrdersServices conn20 = new ConnectorOrdersServices() { Id = 20, OrderId = order8.Id, ServiceId = service5.Id };
                ConnectorOrdersServices conn21 = new ConnectorOrdersServices() { Id = 21, OrderId = order9.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn22 = new ConnectorOrdersServices() { Id = 22, OrderId = order9.Id, ServiceId = service3.Id };
                ConnectorOrdersServices conn23 = new ConnectorOrdersServices() { Id = 23, OrderId = order9.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn24 = new ConnectorOrdersServices() { Id = 24, OrderId = order10.Id, ServiceId = service5.Id };
                ConnectorOrdersServices conn25 = new ConnectorOrdersServices() { Id = 25, OrderId = order11.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn26 = new ConnectorOrdersServices() { Id = 26, OrderId = order11.Id, ServiceId = service5.Id };
                ConnectorOrdersServices conn27 = new ConnectorOrdersServices() { Id = 27, OrderId = order12.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn28 = new ConnectorOrdersServices() { Id = 28, OrderId = order12.Id, ServiceId = service2.Id };
                ConnectorOrdersServices conn29 = new ConnectorOrdersServices() { Id = 29, OrderId = order13.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn30 = new ConnectorOrdersServices() { Id = 30, OrderId = order13.Id, ServiceId = service2.Id };
                ConnectorOrdersServices conn31 = new ConnectorOrdersServices() { Id = 31, OrderId = order13.Id, ServiceId = service3.Id };
                ConnectorOrdersServices conn32 = new ConnectorOrdersServices() { Id = 32, OrderId = order13.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn33 = new ConnectorOrdersServices() { Id = 33, OrderId = order14.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn34 = new ConnectorOrdersServices() { Id = 34, OrderId = order14.Id, ServiceId = service3.Id };
                ConnectorOrdersServices conn35 = new ConnectorOrdersServices() { Id = 35, OrderId = order14.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn36 = new ConnectorOrdersServices() { Id = 36, OrderId = order15.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn37 = new ConnectorOrdersServices() { Id = 37, OrderId = order15.Id, ServiceId = service3.Id };
                ConnectorOrdersServices conn38 = new ConnectorOrdersServices() { Id = 38, OrderId = order15.Id, ServiceId = service5.Id };
                ConnectorOrdersServices conn39 = new ConnectorOrdersServices() { Id = 39, OrderId = order16.Id, ServiceId = service6.Id };
                ConnectorOrdersServices conn40 = new ConnectorOrdersServices() { Id = 40, OrderId = order16.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn41 = new ConnectorOrdersServices() { Id = 41, OrderId = order17.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn42 = new ConnectorOrdersServices() { Id = 42, OrderId = order17.Id, ServiceId = service3.Id };
                ConnectorOrdersServices conn43 = new ConnectorOrdersServices() { Id = 43, OrderId = order17.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn44 = new ConnectorOrdersServices() { Id = 44, OrderId = order18.Id, ServiceId = service1.Id };
                ConnectorOrdersServices conn45 = new ConnectorOrdersServices() { Id = 45, OrderId = order18.Id, ServiceId = service3.Id };
                ConnectorOrdersServices conn46 = new ConnectorOrdersServices() { Id = 46, OrderId = order18.Id, ServiceId = service4.Id };
                ConnectorOrdersServices conn47 = new ConnectorOrdersServices() { Id = 47, OrderId = order18.Id, ServiceId = service5.Id };
                ConnectorOrdersServices conn48 = new ConnectorOrdersServices() { Id = 48, OrderId = order19.Id, ServiceId = service5.Id };
                ConnectorOrdersServices conn49 = new ConnectorOrdersServices() { Id = 49, OrderId = order19.Id, ServiceId = service2.Id };
                ConnectorOrdersServices conn50 = new ConnectorOrdersServices() { Id = 50, OrderId = order19.Id, ServiceId = service3.Id };

                modelBuilder.Entity<Orders>(entity =>
                {
                    entity.HasOne(order => order.Clown)
                         .WithMany(clown => clown.Orders)
                         .HasForeignKey(order => order.ClownId)
                         .OnDelete(DeleteBehavior.SetNull);
                });

                modelBuilder.Entity<Orders>(entity =>
                {
                    entity.HasOne(order => order.Customer)
                         .WithMany(customer => customer.Orders)
                         .HasForeignKey(order => order.CustomerId)
                         .OnDelete(DeleteBehavior.SetNull);
                });

                modelBuilder.Entity<ConnectorOrdersServices>(entity =>
                {
                    entity.HasOne(conn => conn.Orders)
                         .WithMany(order => order.ConnectorOrdersServices)
                         .HasForeignKey(conn => conn.OrderId)
                         .OnDelete(DeleteBehavior.SetNull);
                });

                modelBuilder.Entity<ConnectorOrdersServices>(entity =>
                {
                    entity.HasOne(conn => conn.Services)
                         .WithMany(service => service.ConnectorOrdersServices)
                         .HasForeignKey(conn => conn.ServiceId)
                         .OnDelete(DeleteBehavior.SetNull);
                });

                modelBuilder.Entity<Customers>().HasData(customer1, customer2, customer3, customer4, customer5, customer6, customer7, customer8, customer9, customer10, customer11, customer12, customer13, customer14, customer15);
                modelBuilder.Entity<Clowns>().HasData(clown1, clown2, clown3, clown4, clown5, clown6);
                modelBuilder.Entity<ServiceOptions>().HasData(service1, service2, service3, service4, service5, service6);
                modelBuilder.Entity<Orders>().HasData(order1, order2, order3, order4, order5, order6, order7, order8, order9, order10, order11, order12, order13, order14, order15, order16, order17, order18, order19);
                modelBuilder.Entity<ConnectorOrdersServices>().HasData(conn1, conn2, conn3, conn4, conn5, conn6, conn7, conn8, conn9, conn10, conn11, conn12, conn13, conn14, conn15, conn16, conn17, conn18, conn19, conn20, conn21, conn22, conn23, conn24, conn25, conn26, conn27, conn28, conn29, conn30, conn31, conn32, conn33, conn34, conn35, conn36, conn37, conn38, conn39, conn40, conn41, conn42, conn43, conn44, conn45, conn46, conn47, conn48, conn49, conn50);
        }
    }
}
