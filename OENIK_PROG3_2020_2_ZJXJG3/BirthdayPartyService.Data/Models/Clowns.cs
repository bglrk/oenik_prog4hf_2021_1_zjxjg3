﻿// <copyright file="Clowns.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Clowns table which represents the clowns who can be hired to a birthday party.
    /// </summary>
    [Table("clowns")]
    public class Clowns
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Clowns"/> class.
        /// </summary>
        public Clowns()
        {
            this.Orders = new HashSet<Orders>();
        }

        /// <summary>
        /// Gets or sets the primary key of the clowns object.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the clown object.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the stage name of the clown object.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string StageName { get; set; }

        /// <summary>
        /// Gets or sets the gender (M/F/N) of the clown object.
        /// </summary>
        [MaxLength(1)]
        [Required]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the hours the clown works.
        /// </summary>
        [Required]
        public int Duration { get; set; }

        /// <summary>
        /// Gets or sets the price for the clown.
        /// </summary>
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets the Orders collection.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Orders> Orders { get; }

        /// <summary>
        /// Writes out data in string format.
        /// </summary>
        /// <returns> Returns a string with all needed information. </returns>
        public override string ToString()
        {
            return $"\n{this.Id,3} | {this.StageName,-10} {this.Duration} hours {this.Price,10} USD {this.Gender,10}\t {this.Name,-1}";
        }
    }
}