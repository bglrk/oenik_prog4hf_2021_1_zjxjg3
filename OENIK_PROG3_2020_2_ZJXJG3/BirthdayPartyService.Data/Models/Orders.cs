﻿// <copyright file="Orders.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Orders class which represent the ordered services and the chosen clown for the party.
    /// </summary>
    [Table("orders")]
    public class Orders
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Orders"/> class.
        /// </summary>
        public Orders()
        {
            this.ConnectorOrdersServices = new HashSet<ConnectorOrdersServices>();
        }

        /// <summary>
        /// Gets or sets the primary key of the order object.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the datetime of the order object.
        /// </summary>
        [Required]
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Gets or sets the customer object.
        /// </summary>
        [NotMapped]
        public virtual Customers Customer { get; set; }

        /// <summary>
        /// Gets or sets the CustomerId for the customer object. It is a foreign key.
        /// </summary>
        [ForeignKey(nameof(Customer))]
        public int? CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the clown object.
        /// </summary>
        [NotMapped]
        public virtual Clowns Clown { get; set; }

        /// <summary>
        /// Gets or sets the ClownId for the clown object. It is a foreign key.
        /// </summary>
        [ForeignKey(nameof(Clown))]
        public int? ClownId { get; set; }

        /// <summary>
        /// Gets the ConnectorOrdersServices collection.
        /// </summary>
        [NotMapped]
        public virtual ICollection<ConnectorOrdersServices> ConnectorOrdersServices { get; }

        /// <summary>
        /// Writes out data in string format.
        /// </summary>
        /// <returns> Returns a string with all needed information. </returns>
        public override string ToString()
        {
            return $"\n{this.Id,3} | {this.Customer?.Name ?? "N/A",-20} {this.DateTime.Year,10}.{this.DateTime.Month}.{this.DateTime.Day} \t{this.Clown?.StageName ?? "N/A",-10} {this.ConnectorOrdersServices?.Count ?? 0,10}";
        }
    }
}
