﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "This is a silly warning!", Scope = "member", Target = "~M:BirthdayPartyService.Data.Models.BirthdayPartyServiceDatabaseContext.OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder)")]