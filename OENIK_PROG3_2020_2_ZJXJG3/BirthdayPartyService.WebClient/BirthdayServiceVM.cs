﻿// <copyright file="BirthdayServiceVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WebClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This is the BirthdayServiceVM class.
    /// </summary>
    public class BirthdayServiceVM : ObservableObject
    {
        private int id;
        private string servicename;
        private int price;
        private int minage;
        private int maxage;
        private int reccomendation;
        private bool isindoor;

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string ServiceName
        {
            get { return this.servicename; }
            set { this.Set(ref this.servicename, value); }
        }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public int Price
        {
            get { return this.price; }
            set { this.Set(ref this.price, value); }
        }

        /// <summary>
        /// Gets or sets the min. age.
        /// </summary>
        public int MinAge
        {
            get { return this.minage; }
            set { this.Set(ref this.minage, value); }
        }

        /// <summary>
        /// Gets or sets the max. age.
        /// </summary>
        public int MaxAge
        {
            get { return this.maxage; }
            set { this.Set(ref this.maxage, value); }
        }

        /// <summary>
        /// Gets or sets the recommendation.
        /// </summary>
        public int Recommendation
        {
            get { return this.reccomendation; }
            set { this.Set(ref this.reccomendation, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the indoor is true.
        /// </summary>
        public bool IsIndoor
        {
            get { return this.isindoor; }
            set { this.Set(ref this.isindoor, value); }
        }

        /// <summary>
        /// This is the copyFrom method.
        /// </summary>
        /// <param name="other">The parameter is another birthdayserviceVM object.</param>
        public void CopyFrom(BirthdayServiceVM other)
        {
            if (other == null)
            {
                return;
            }

            this.Id = other.Id;
            this.ServiceName = other.ServiceName;
            this.Price = other.Price;
            this.MinAge = other.MinAge;
            this.MaxAge = other.MaxAge;
            this.Recommendation = other.Recommendation;
            this.IsIndoor = other.IsIndoor;
        }
    }
}
