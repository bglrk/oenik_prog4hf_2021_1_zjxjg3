﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WebClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// This is the MainLogic class.
    /// </summary>
    public class MainLogic
    {
        private string url = "http://localhost:62385/BirthdayServicesApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// This is the ApiGetServices method.
        /// </summary>
        /// <returns>Returns an Icollection of BirthdayServiceVM objects.</returns>
        public ICollection<BirthdayServiceVM> ApiGetServices()
        {
            // it was a List before changing
            string json = this.client.GetStringAsync(new Uri(this.url + "all")).Result;
            var list = JsonSerializer.Deserialize<List<BirthdayServiceVM>>(json, this.jsonOptions);
            return list;
        }

        /// <summary>
        /// This is the ApiDelService method.
        /// </summary>
        /// <param name="service">The parameter is a birthdayservicevm object.</param>
        public void ApiDelService(BirthdayServiceVM service)
        {
            bool success = false;
            if (service != null)
            {
                string json = this.client.GetStringAsync(new Uri(this.url + "del/" + service.Id.ToString(CultureInfo.CurrentCulture))).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// This is the ApiEditService method.
        /// </summary>
        /// <param name="service">The parameter is a birthdayservicevm object.</param>
        /// <param name="isEditing">The parameter is a boolean.</param>
        /// <returns>Returns true if success.</returns>
        public bool ApiEditService(BirthdayServiceVM service, bool isEditing)
        {
            if (service == null)
            {
                return false;
            }

            string myUrl = isEditing ? this.url + "mod" : this.url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", service.Id.ToString(CultureInfo.CurrentCulture));
            }

            postData.Add("serviceName", service.ServiceName);
            postData.Add("price", service.Price.ToString(CultureInfo.CurrentCulture));
            postData.Add("minAge", service.MinAge.ToString(CultureInfo.CurrentCulture));
            postData.Add("maxAge", service.MaxAge.ToString(CultureInfo.CurrentCulture));
            postData.Add("recommendation", service.Recommendation.ToString(CultureInfo.CurrentCulture));
            postData.Add("isIndoor", service.IsIndoor.ToString(CultureInfo.CurrentCulture));

            string json = this.client.PostAsync(new Uri(myUrl), new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// This is the EditService method.
        /// </summary>
        /// <param name="service">The parameter is a birthdayservicevm object.</param>
        /// <param name="editor">The parameter is a func.</param>
        public void EditService(BirthdayServiceVM service, Func<BirthdayServiceVM, bool> editor)
        {
            BirthdayServiceVM clone = new BirthdayServiceVM();
            if (service != null)
            {
                clone.CopyFrom(service);
            }

            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (service != null)
                {
                    success = this.ApiEditService(clone, true);
                }
                else
                {
                    success = this.ApiEditService(clone, false);
                }
            }

            SendMessage(success == true);
        }

        /// <summary>
        /// This is the SendMessage method.
        /// </summary>
        /// <param name="success">The parameter is a boolean.</param>
        private static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "ServiceResult");
        }
    }
}
