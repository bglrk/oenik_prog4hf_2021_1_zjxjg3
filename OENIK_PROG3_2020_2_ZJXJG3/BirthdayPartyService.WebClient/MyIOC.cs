﻿// <copyright file="MyIOC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WebClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// MyIoc.
    /// </summary>
    internal class MyIOC : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets instances.
        /// </summary>
        public static MyIOC Intstance { get; private set; } = new MyIOC();
    }
}
