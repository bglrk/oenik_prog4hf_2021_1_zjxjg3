﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WebClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// This is the MainVM class.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private ObservableCollection<BirthdayServiceVM> allServices;
        private BirthdayServiceVM selectedService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">Logic.</param>
        public MainVM(MainLogic logic)
        {
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() => this.AllServices = new ObservableCollection<BirthdayServiceVM>(this.logic.ApiGetServices()));
            this.AddCmd = new RelayCommand(() => this.logic.EditService(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditService(this.SelectedService, this.EditorFunc));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelService(this.SelectedService));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<MainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the selected service.
        /// </summary>
        public BirthdayServiceVM SelectedService
        {
            get { return this.selectedService; }
            set { this.Set(ref this.selectedService, value); }
        }

        /// <summary>
        /// Gets the all services.
        /// </summary>
        public ObservableCollection<BirthdayServiceVM> AllServices
        {
            get { return this.allServices; }
            private set { this.Set(ref this.allServices, value); }
        }

        /// <summary>
        /// Gets or sets editor func.
        /// </summary>
        public Func<BirthdayServiceVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets add.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets del.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets mod.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
