﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BirthdayPartyService.Data;
    using BirthdayPartyService.Data.Models;
    using BirthdayPartyService.Logic;
    using BirthdayPartyService.Repo;

    /// <summary>
    /// This is the Factory class.
    /// </summary>
    public class Factory
    {
        private BirthdayPartyServiceDatabaseContext ctx;
        private CustomersRepository customerRepo;
        private ClownsRepository clownRepo;
        private OrdersRepository orderRepo;
        private ServicesRepository serviceRepo;
        private ConnectorRepository connectorRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// </summary>
        public Factory()
        {
            this.ctx = new BirthdayPartyServiceDatabaseContext();
            this.customerRepo = new CustomersRepository(this.ctx);
            this.clownRepo = new ClownsRepository(this.ctx);
            this.orderRepo = new OrdersRepository(this.ctx);
            this.serviceRepo = new ServicesRepository(this.ctx);
            this.connectorRepo = new ConnectorRepository(this.ctx);
            this.VisibleLogic = new VisibleContentLogic(this.clownRepo, this.serviceRepo);
            this.ClientLogic = new ClientLogic(this.orderRepo, this.customerRepo, this.connectorRepo);
            this.CompanyLogic = new CompanyLogic(this.orderRepo, this.customerRepo, this.clownRepo, this.serviceRepo, this.connectorRepo);
        }

        /// <summary>
        /// Gets the VisibleLogic.
        /// </summary>
        public VisibleContentLogic VisibleLogic { get; }

        /// <summary>
        /// Gets the ClientLogic.
        /// </summary>
        public ClientLogic ClientLogic { get; }

        /// <summary>
        /// Gets the CompanyLogic.
        /// </summary>
        public CompanyLogic CompanyLogic { get; }
    }
}
