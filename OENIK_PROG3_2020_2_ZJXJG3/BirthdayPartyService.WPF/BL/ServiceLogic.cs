﻿// <copyright file="ServiceLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BirthdayPartyService.Data.Models;
    using BirthdayPartyService.WPF.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// This is the ServiceLogic class.
    /// </summary>
    public class ServiceLogic : IServiceLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Factory factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceLogic"/> class.
        /// </summary>
        /// <param name="editorService">The parameter is an IEditorService object.</param>
        /// <param name="messengerService">The parameter is an IMessenger object.</param>
        /// <param name="factory">The parameter is a Factory.</param>
        public ServiceLogic(IEditorService editorService, IMessenger messengerService, Factory factory)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.factory = factory;
        }

        /// <summary>
        /// AddService method.
        /// </summary>
        /// <param name="list">The parameter is an IList of BirthdayService objects.</param>
        public void AddService(IList<BirthdayService> list)
        {
            BirthdayService newService = new BirthdayService();
            if (this.editorService.EditService(newService) == true)
            {
                // Call DB operation
                // myLogic.AddEntity(xxx);
                newService.Id = this.factory.CompanyLogic.InsertNewService(newService.Name, newService.Price, newService.MinAge, newService.MaxAge, newService.Recommendation, newService.IsIndoor).Id;
                list.Add(newService);
                this.messengerService.Send("Adding was successfull!", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Adding failed.", "LogicResult");
            }
        }

        /// <summary>
        /// DeleteService method.
        /// </summary>
        /// <param name="list">The parameter is an IList of BirthdayService objects.</param>
        /// <param name="service">The parameter is a BirthdayService object.</param>
        public void DeleteService(IList<BirthdayService> list, BirthdayService service)
        {
            if (service != null && list.Remove(service))
            {
                // Call DB operation
                this.factory.CompanyLogic.DeleteService(service.Id);
                this.messengerService.Send("Deleting was successfull!", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Deleting failed.", "LogicResult");
            }
        }

        /// <summary>
        /// EditService method.
        /// </summary>
        /// <param name="serviceToModify">The parameter is a BirthdayService object.</param>
        public void EditService(BirthdayService serviceToModify)
        {
            if (serviceToModify == null)
            {
                this.messengerService.Send("Editing failed.", "LogicResult");
                return;
            }

            BirthdayService clone = new BirthdayService();
            clone.CopyFrom(serviceToModify);
            if (this.editorService.EditService(clone) == true)
            {
                serviceToModify.CopyFrom(clone);

                // Call DB operation
                ServiceOptions service = new ServiceOptions();
                service.Id = serviceToModify.Id;
                service.Name = serviceToModify.Name;
                service.Price = serviceToModify.Price;
                service.RecommendedMinAge = serviceToModify.MinAge;
                service.RecommendedMaxAge = serviceToModify.MaxAge;
                service.Indoor = serviceToModify.IsIndoor;
                service.Recommendation = serviceToModify.Recommendation;
                this.factory.CompanyLogic.ChangeServiceFull(serviceToModify.Id, service);
                this.messengerService.Send("Editing was successfull!", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Editing failed.", "LogicResult");
            }
        }

        /// <summary>
        /// GetAllServices method.
        /// </summary>
        /// <returns>Returns an IList of BirthdayServices.</returns>
        public IList<BirthdayService> GetAllServices()
        {
            IList<BirthdayService> returnList = new List<BirthdayService>();
            IList<ServiceOptions> repoList = this.factory.VisibleLogic.GetAllServices();
            foreach (var item in repoList)
            {
                BirthdayService service = new BirthdayService();
                service.Id = item.Id;
                service.Name = item.Name;
                service.Price = item.Price;
                service.MinAge = item.RecommendedMinAge;
                service.MaxAge = item.RecommendedMaxAge;
                service.Recommendation = item.Recommendation;
                service.IsIndoor = item.Indoor;

                returnList.Add(service);
            }

            return returnList;
        }
    }
}
