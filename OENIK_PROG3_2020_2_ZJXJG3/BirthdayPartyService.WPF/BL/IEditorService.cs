﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BirthdayPartyService.WPF.Data;

    /// <summary>
    /// This is the IEditorService interface.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// It is a boolean.
        /// </summary>
        /// <param name="service">The parameter is a BirthdayService object.</param>
        /// <returns>Returns true or false.</returns>
        bool EditService(BirthdayService service);
    }
}
