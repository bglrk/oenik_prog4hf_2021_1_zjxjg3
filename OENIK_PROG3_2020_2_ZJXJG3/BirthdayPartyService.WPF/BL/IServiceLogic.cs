﻿// <copyright file="IServiceLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BirthdayPartyService.WPF.Data;

    /// <summary>
    /// This is the IServiceLogic interface.
    /// </summary>
    public interface IServiceLogic
    {
        /// <summary>
        /// AddService method.
        /// </summary>
        /// <param name="list">The parameter is an IList of BirthdayService objects.</param>
        void AddService(IList<BirthdayService> list);

        /// <summary>
        /// EditService method.
        /// </summary>
        /// <param name="serviceToModify">The parameter is a BirthdayService object.</param>
        void EditService(BirthdayService serviceToModify);

        /// <summary>
        /// DeleteService method.
        /// </summary>
        /// <param name="list">The parameter is an IList of BirthdayService objects.</param>
        /// <param name="service">The parameter is a BirthdayService object.</param>
        void DeleteService(IList<BirthdayService> list, BirthdayService service);

        /// <summary>
        /// GetAllServices method.
        /// </summary>
        /// <returns>Returns an IList of BirthdayServices.</returns>
        IList<BirthdayService> GetAllServices();
    }
}
