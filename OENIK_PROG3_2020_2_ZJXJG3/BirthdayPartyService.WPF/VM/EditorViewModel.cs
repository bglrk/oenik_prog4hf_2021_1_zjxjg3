﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BirthdayPartyService.WPF.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This is the EditorViewModel class.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private BirthdayService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.service = new BirthdayService();
            if (this.IsInDesignMode)
            {
                this.service.Name = "Test service";
                this.service.Price = 420;
                this.service.MinAge = 6;
                this.service.MaxAge = 9;
                this.service.IsIndoor = true;
                this.service.Recommendation = 8;
            }
        }

        /// <summary>
        /// Gets the array of valid ages.
        /// </summary>
        public Array ChildAges
        {
            get
            {
                int[] array = new int[14];
                for (int i = 0; i < 14; i++)
                {
                    array[i] = i + 5;
                }

                return array;
            }
        }

        /// <summary>
        /// Gets or sets the BirthdayService object.
        /// </summary>
        public BirthdayService Service
        {
            get { return this.service; }
            set { this.Set(ref this.service, value); }
        }
    }
}
