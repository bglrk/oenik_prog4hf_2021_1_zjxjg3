﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using BirthdayPartyService.WPF.BL;
    using BirthdayPartyService.WPF.Data;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// This is the MainViewModel class.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IServiceLogic logic;

        private BirthdayService selectedService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">The parameter is an IServiceLogic object.</param>
        public MainViewModel(IServiceLogic logic)
        {
            this.logic = logic;
            this.BirthdayServices = new ObservableCollection<BirthdayService>();

            // logic.GetAllPlayers() <-- to access the database and make it the collection
            if (this.IsInDesignMode)
            {
                BirthdayService service2 = new BirthdayService()
                {
                    Name = "This is some random text",
                    Price = 133,
                    MinAge = 13,
                    MaxAge = 18,
                    IsIndoor = false,
                    Recommendation = 2,
                };

                BirthdayService service3 = new BirthdayService()
                {
                    Name = "The lazy dog jumped over the quick brown fox",
                    Price = 100,
                    MinAge = 5,
                    MaxAge = 11,
                    IsIndoor = true,
                    Recommendation = 5,
                };

                this.BirthdayServices.Add(service2);
                this.BirthdayServices.Add(service3);
            }
            else
            {
                IList<BirthdayService> lista = this.logic.GetAllServices();
                foreach (var item in lista)
                {
                    this.BirthdayServices.Add(item);
                }
            }

            this.AddCmd = new RelayCommand(() => this.logic.AddService(this.BirthdayServices));
            this.EditCmd = new RelayCommand(() => this.logic.EditService(this.SelectedService));
            this.DeleteCmd = new RelayCommand(() => this.logic.DeleteService(this.BirthdayServices, this.SelectedService));
        }

        // IoC container below

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IServiceLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the selected service object.
        /// </summary>
        public BirthdayService SelectedService
        {
            get { return this.selectedService; }
            set { this.Set(ref this.selectedService, value); }
        }

        /// <summary>
        /// Gets the collection of birthday services.
        /// </summary>
        public ObservableCollection<BirthdayService> BirthdayServices
        {
            get;
            private set;
        }

        // interaction possibilities

        /// <summary>
        /// Gets the command for adding.
        /// </summary>
        public ICommand AddCmd
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the command for editing.
        /// </summary>
        public ICommand EditCmd
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the command for deleting.
        /// </summary>
        public ICommand DeleteCmd
        {
            get;
            private set;
        }
    }
}
