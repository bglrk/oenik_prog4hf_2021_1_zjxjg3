﻿// <copyright file="IndoorBooleanToStringConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// This is the IndoorBooleanToStringConverter class.
    /// </summary>
    public class IndoorBooleanToStringConverter : IValueConverter
    {
        /// <summary>
        /// This is the Convert method.
        /// </summary>
        /// <param name="value">The parameter is an object.</param>
        /// <param name="targetType">The parameter is a Type.</param>
        /// <param name="parameter">The parameter is another object.</param>
        /// <param name="culture">The parameter is a CultureInfo object.</param>
        /// <returns>It returns a string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isIndoor = (bool)value;
            if (isIndoor)
            {
                return string.Format("It can be held indoor");
            }
            else
            {
                return string.Format("It cannot be held indoor");
            }
        }

        /// <summary>
        /// This is the ConvertBack method.
        /// </summary>
        /// <param name="value">The parameter is an object.</param>
        /// <param name="targetType">The parameter is a Type object.</param>
        /// <param name="parameter">The parameter is another object.</param>
        /// <param name="culture">The parameter is a CultureInfo object.</param>
        /// <returns>Returns a boolean.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string input = value.ToString();
                if (input == "It can be held indoor")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
