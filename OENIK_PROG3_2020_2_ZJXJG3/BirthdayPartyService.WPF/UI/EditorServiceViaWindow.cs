﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BirthdayPartyService.WPF.BL;
    using BirthdayPartyService.WPF.Data;

    /// <summary>
    /// This is the class of EditorServiceViaWindow.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// This is the EditService method.
        /// </summary>
        /// <param name="service">The parameter is a BirthdayService object.</param>
        /// <returns>Returns true or false.</returns>
        public bool EditService(BirthdayService service)
        {
            EditorWindow win = new EditorWindow(service);
            return win.ShowDialog() ?? false;
        }
    }
}
