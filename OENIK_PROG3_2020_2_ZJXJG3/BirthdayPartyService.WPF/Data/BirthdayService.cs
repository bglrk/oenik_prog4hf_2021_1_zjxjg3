﻿// <copyright file="BirthdayService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.WPF.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This is the BirthdayService class for the Data folder.
    /// </summary>
    public class BirthdayService : ObservableObject
    {
        private int id;
        private string name;
        private int price;
        private int minAge;
        private int maxAge;
        private bool isIndoor;
        private int recommendation;

        /// <summary>
        /// Gets or sets the id of the service.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the name of the service.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the price of the service.
        /// </summary>
        public int Price
        {
            get { return this.price; }
            set { this.Set(ref this.price, value); }
        }

        /// <summary>
        /// Gets or sets the minimum recommended age of the service.
        /// </summary>
        public int MinAge
        {
            get { return this.minAge; }
            set { this.Set(ref this.minAge, value); }
        }

        /// <summary>
        /// Gets or sets the maximum recommended age of the service.
        /// </summary>
        public int MaxAge
        {
            get { return this.maxAge; }
            set { this.Set(ref this.maxAge, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the service can be held indoor.
        /// </summary>
        public bool IsIndoor
        {
            get { return this.isIndoor; }
            set { this.Set(ref this.isIndoor, value); }
        }

        /// <summary>
        /// Gets or sets the recommendation rating of the service.
        /// </summary>
        public int Recommendation
        {
            get { return this.recommendation; }
            set { this.Set(ref this.recommendation, value); }
        }

        /// <summary>
        /// Creates a shallow copy of a BirthdayService object.
        /// </summary>
        /// <param name="other">The parameter is a BirthdayService object.</param>
        public void CopyFrom(BirthdayService other)
        {
            if (other != null)
            {
                // this.name = other.name;
                // this.price = other.price;
                // this.minAge = other.minAge;
                // this.maxAge = other.maxAge;
                // this.isIndoor = other.isIndoor;
                // this.recommendation = other.recommendation;
                this.GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
            }

            // reflection is slow
            // this.GetType().GetProperties().ToList().ForEach(
            //    property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
