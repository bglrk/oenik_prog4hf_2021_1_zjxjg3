﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1115:Parameter should follow comma", Justification = "silly thing.")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "i dont agree", Scope = "member", Target = "~M:BirthdayPartyService.WPF.BL.ServiceLogic.AddService(System.Collections.Generic.IList{BirthdayPartyService.WPF.Data.BirthdayService})")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "i dont agree", Scope = "member", Target = "~M:BirthdayPartyService.WPF.BL.ServiceLogic.DeleteService(System.Collections.Generic.IList{BirthdayPartyService.WPF.Data.BirthdayService},BirthdayPartyService.WPF.Data.BirthdayService)")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "idk", Scope = "member", Target = "~P:BirthdayPartyService.WPF.VM.EditorViewModel.ChildAges")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:Field names should begin with lower-case letter", Justification = "idc", Scope = "member", Target = "~F:BirthdayPartyService.WPF.UI.EditorWindow.VM")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "it's fine", Scope = "member", Target = "~M:BirthdayPartyService.WPF.UI.IndoorBooleanToStringConverter.Convert(System.Object,System.Type,System.Object,System.Globalization.CultureInfo)~System.Object")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1306:Field names should begin with lower-case letter", Justification = "i love uppercase", Scope = "member", Target = "~F:BirthdayPartyService.WPF.MainWindow.VM")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "i think its okay", Scope = "type", Target = "~T:BirthdayPartyService.WPF.BL.Factory")]