﻿// <copyright file="ConnectorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BirthdayPartyService.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This is the orders repository class.
    /// </summary>
    public class ConnectorRepository : Repository<ConnectorOrdersServices>, IConnectorRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectorRepository"/> class.
        /// </summary>
        /// <param name="ctx">The parameter is a DbContext.</param>
        public ConnectorRepository(DbContext ctx)
                : base(ctx)
        {
        }

        /// <summary>
        /// This method will find an object by the id.
        /// </summary>
        /// <param name="id">The parameter is an id.</param>
        /// <returns>Returns a single ConnectorOrdersServices type record. </returns>
        public override ConnectorOrdersServices GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// This method removes a record from the database.
        /// </summary>
        /// <param name="id">The parameter is an int.</param>
        public override void Remove(int id)
        {
            ConnectorOrdersServices obj = this.GetOne(id);
            this.Ctx.Set<ConnectorOrdersServices>().Remove(obj);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method adds a new record to the database.
        /// </summary>
        /// <param name="entity">The parameter is a serviceoptions object.</param>
        public override void Insert(ConnectorOrdersServices entity)
        {
            this.Ctx.Set<ConnectorOrdersServices>().Add(entity);
            this.Ctx.SaveChanges();
        }
    }
}
