﻿// <copyright file="OrdersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BirthdayPartyService.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This is the orders repository class.
    /// </summary>
    public class OrdersRepository : Repository<Orders>, IOrdersRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersRepository"/> class.
        /// </summary>
        /// <param name="ctx">The parameter is a DbContext.</param>
        public OrdersRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// This method can change the datetime for an order.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newDate">This is the new date time. </param>
        public void ChangeDate(int id, DateTime newDate)
        {
            var order = this.GetOne(id);
            if (order == null)
            {
                throw new InvalidOperationException("Order not found!");
            }

            order.DateTime = newDate;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method will find an object by the id.
        /// </summary>
        /// <param name="id">The parameter is an id.</param>
        /// <returns>Returns a single Orders type record. </returns>
        public override Orders GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// This method removes a record from the database.
        /// </summary>
        /// <param name="id">The parameter is an int.</param>
        public override void Remove(int id)
        {
            Orders obj = this.GetOne(id);
            this.Ctx.Set<Orders>().Remove(obj);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method adds a new record to the database.
        /// </summary>
        /// <param name="entity">The parameter is a serviceoptions object.</param>
        public override void Insert(Orders entity)
        {
            this.Ctx.Set<Orders>().Add(entity);
            this.Ctx.SaveChanges();
        }
    }
}
