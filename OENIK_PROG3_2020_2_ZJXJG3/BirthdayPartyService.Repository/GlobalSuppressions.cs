﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "This is a silly warning!", Scope = "member", Target = "~F:BirthdayPartyService.Repo.Repository`1.ctx")]
[assembly: SuppressMessage("Design", "CA1012:Abstract types should not have public constructors", Justification = "This is a silly warning!", Scope = "type", Target = "~T:BirthdayPartyService.Repo.Repository`1")]
