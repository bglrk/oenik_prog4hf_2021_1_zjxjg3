﻿// <copyright file="IClownsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BirthdayPartyService.Data.Models;

    /// <summary>
    /// This is the interface for the clowns repository.
    /// </summary>
    public interface IClownsRepository : IRepository<Clowns>
    {
        /// <summary>
        /// This method can change the stage name of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newName">This is the new name of the clown. </param>
        void ChangeStageName(int id, string newName);

        /// <summary>
        /// This method can change the price of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newPrice">This is the new price of the clown. </param>
        void ChangePrice(int id, int newPrice);

        /// <summary>
        /// This method can change the working hours of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newHours">This is how many hours will work the clown. </param>
        void ChangeWorkingHours(int id, int newHours);
    }
}
