﻿// <copyright file="IServicesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BirthdayPartyService.Data.Models;

    /// <summary>
    /// This is the interface for the services repository.
    /// </summary>
    public interface IServicesRepository : IRepository<ServiceOptions>
    {
        /// <summary>
        /// This method will change the price of a service.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newPrice">This is the new price of the service. </param>
        void ChangePrice(int id, int newPrice);

        /// <summary>
        /// This method will change the recommendation rating of a service.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newRating">This is the new recommendation rating of the service. </param>
        void UpdateRecommendation(int id, int newRating);

        /// <summary>
        /// This is the UpdateServiceFull method.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <param name="newService">The parameter is a Service object.</param>
        void UpdateServiceFull(int id, ServiceOptions newService);
    }
}
