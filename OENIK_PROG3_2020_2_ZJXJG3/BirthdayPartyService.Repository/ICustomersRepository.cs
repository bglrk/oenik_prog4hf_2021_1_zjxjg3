﻿// <copyright file="ICustomersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BirthdayPartyService.Data.Models;

    /// <summary>
    /// This is the interface for the customers repository.
    /// </summary>
    public interface ICustomersRepository : IRepository<Customers>
    {
        /// <summary>
        /// This method will change the address of a customer.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newCity">This is the new city of the customer. </param>
        /// <param name="newAddress">This is the new address of the customer. </param>
        void ChangeAddress(int id, string newCity, string newAddress);

        /// <summary>
        /// This method will change the email address of a customer.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newEmail">This is the new email address of the customer. </param>
        void ChangeEmail(int id, string newEmail);

        /// <summary>
        /// This method will change the number of participants.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newNum">This is the number of kids. </param>
        void ChangeNumOfKids(int id, int newNum);
    }
}
