﻿// <copyright file="ClownsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BirthdayPartyService.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This is the clowns repository class.
    /// </summary>
    public class ClownsRepository : Repository<Clowns>, IClownsRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClownsRepository"/> class.
        /// </summary>
        /// <param name="ctx">The parameter is a DbContext. </param>
        public ClownsRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// This method can change the price of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newPrice">This is the new price of the clown. </param>
        public void ChangePrice(int id, int newPrice)
        {
            var clown = this.GetOne(id);
            if (clown == null)
            {
                throw new InvalidOperationException("Clown not found!");
            }

            clown.Price = newPrice;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method can change the stage name of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newName">This is the new name of the clown. </param>
        public void ChangeStageName(int id, string newName)
        {
            var clown = this.GetOne(id);
            if (clown == null)
            {
                throw new InvalidOperationException("Clown not found!");
            }

            clown.StageName = newName;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method can change the working hours of a clown.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newHours">This is how many hours will work the clown. </param>
        public void ChangeWorkingHours(int id, int newHours)
        {
            var clown = this.GetOne(id);
            if (clown == null)
            {
                throw new InvalidOperationException("Clown not found!");
            }

            clown.Duration = newHours;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method will find an object by the id.
        /// </summary>
        /// <param name="id">The parameter is an id. </param>
        /// <returns>Returns a single Clowns type record.</returns>
        public override Clowns GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// This method removes a record from the database.
        /// </summary>
        /// <param name="id">The parameter is an int.</param>
        public override void Remove(int id)
        {
            Clowns obj = this.GetOne(id);
            this.Ctx.Set<Clowns>().Remove(obj);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method adds a new record to the database.
        /// </summary>
        /// <param name="entity">The parameter is a serviceoptions object.</param>
        public override void Insert(Clowns entity)
        {
            this.Ctx.Set<Clowns>().Add(entity);
            this.Ctx.SaveChanges();
        }
    }
}
