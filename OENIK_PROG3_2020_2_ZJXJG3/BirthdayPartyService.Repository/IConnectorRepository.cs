﻿// <copyright file="IConnectorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BirthdayPartyService.Data.Models;

    /// <summary>
    /// This is the interface for the connector repository.
    /// </summary>
    public interface IConnectorRepository : IRepository<ConnectorOrdersServices>
    {
    }
}
