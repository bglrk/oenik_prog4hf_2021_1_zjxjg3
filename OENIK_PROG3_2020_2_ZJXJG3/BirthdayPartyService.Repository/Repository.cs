﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This is the generic repository class.
    /// </summary>
    /// <typeparam name="T">Type T represents a class. </typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// This is the DbContext field.
        /// </summary>
        private DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="ctx">The parameter is a DbContext. </param>
        public Repository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Gets or sets DbContext field so the child can reach it.
        /// </summary>
        protected DbContext Ctx { get => this.ctx; set => this.ctx = value; }

        /// <summary>
        /// This is the GetAll method.
        /// </summary>
        /// <returns>It returns every record as a list. </returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <summary>
        /// This method will find an object by the id.
        /// </summary>
        /// <param name="id">The parameter named id which we are looking for. </param>
        /// <returns>Returns a single T type record.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// This method can insert a record into the database.
        /// </summary>
        /// <param name="entity">The parameter is a class object.</param>
        public abstract void Insert(T entity);

        /// <summary>
        /// This method can remove a record from the database.
        /// </summary>
        /// <param name="id">The parameter is an int.</param>
        public abstract void Remove(int id);
    }
}
