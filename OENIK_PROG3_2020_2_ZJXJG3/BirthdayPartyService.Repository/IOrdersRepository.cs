﻿// <copyright file="IOrdersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BirthdayPartyService.Data.Models;

    /// <summary>
    /// This is the interface for the orders repository.
    /// </summary>
    public interface IOrdersRepository : IRepository<Orders>
    {
        /// <summary>
        /// This method will change the date of an order.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newDate">This is the new datetime of the order. </param>
        void ChangeDate(int id, DateTime newDate);
    }
}
