﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This is the IRepository class.
    /// </summary>
    /// <typeparam name="T"> Type T represents a class. </typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// This method will find an object by the id.
        /// </summary>
        /// <param name="id">The parameter named id which we are looking for. </param>
        /// <returns>Returns a single T type record. </returns>
        T GetOne(int id);

        /// <summary>
        /// This method will list all records from the database.
        /// </summary>
        /// <returns> Returns every T type record. </returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// This method can insert a record into the database.
        /// </summary>
        /// <param name="entity">The parameter is an object. </param>
        void Insert(T entity);

        /// <summary>
        /// This method can remove a record from the database.
        /// </summary>
        /// <param name="id">The parameter is an int. </param>
        void Remove(int id);
    }
}
