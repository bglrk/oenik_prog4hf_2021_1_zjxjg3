﻿// <copyright file="ServicesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BirthdayPartyService.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This is the services repository class.
    /// </summary>
    public class ServicesRepository : Repository<ServiceOptions>, IServicesRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServicesRepository"/> class.
        /// </summary>
        /// <param name="ctx">The parameter is a DbContext object. </param>
        public ServicesRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// This method will change the price of a service.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newPrice">This is the new price of the service. </param>
        public void ChangePrice(int id, int newPrice)
        {
            var service = this.GetOne(id);
            if (service == null)
            {
                throw new InvalidOperationException("Service not found!");
            }

            service.Price = newPrice;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method will find an object by the id.
        /// </summary>
        /// <param name="id">The parameter is an id. </param>
        /// <returns>Returns a single Servicesx type record.</returns>
        public override ServiceOptions GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// This method will change the recommendation rating of a service.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newRating">This is the new recommendation rating of the service. </param>
        public void UpdateRecommendation(int id, int newRating)
        {
            this.GetOne(id).Recommendation = newRating;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This is the UpdateServiceFull method.
        /// </summary>
        /// <param name="id">The parameter is an ID.</param>
        /// <param name="newService">The parameter is a Service object.</param>
        public void UpdateServiceFull(int id, ServiceOptions newService)
        {
            if (newService != null)
            {
                this.GetOne(id).Name = newService.Name;
                this.GetOne(id).Price = newService.Price;
                this.GetOne(id).RecommendedMinAge = newService.RecommendedMinAge;
                this.GetOne(id).RecommendedMaxAge = newService.RecommendedMaxAge;
                this.GetOne(id).Indoor = newService.Indoor;
                this.GetOne(id).Recommendation = newService.Recommendation;
                this.Ctx.SaveChanges();
            }
        }

        /// <summary>
        /// This method removes a record from the database.
        /// </summary>
        /// <param name="id">The parameter is an int.</param>
        public override void Remove(int id)
        {
            ServiceOptions obj = this.GetOne(id);
            this.Ctx.Set<ServiceOptions>().Remove(obj);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method adds a new record to the database.
        /// </summary>
        /// <param name="entity">The parameter is a serviceoptions object.</param>
        public override void Insert(ServiceOptions entity)
        {
            this.Ctx.Set<ServiceOptions>().Add(entity);
            this.Ctx.SaveChanges();
        }
    }
}
