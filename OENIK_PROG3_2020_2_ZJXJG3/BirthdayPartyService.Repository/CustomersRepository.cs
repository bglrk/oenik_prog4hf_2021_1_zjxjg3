﻿// <copyright file="CustomersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BirthdayPartyService.Repo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BirthdayPartyService.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This is the customers repository class.
    /// </summary>
    public class CustomersRepository : Repository<Customers>, ICustomersRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersRepository"/> class.
        /// </summary>
        /// <param name="ctx">The parameter is a DbContext object. </param>
        public CustomersRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// This method will change the address of a customer.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newCity">This is the new city of the customer. </param>
        /// <param name="newAddress">This is the new address of the customer. </param>
        public void ChangeAddress(int id, string newCity, string newAddress)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Customer not found!");
            }

            customer.City = newCity;
            customer.Address = newAddress;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method will change the email address of a customer.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newEmail">This is the new email address of the customer. </param>
        public void ChangeEmail(int id, string newEmail)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Customer not found!");
            }

            customer.Email = newEmail;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method will change the number of participants.
        /// </summary>
        /// <param name="id">This is the id parameter. </param>
        /// <param name="newNum">This is the number of kids. </param>
        public void ChangeNumOfKids(int id, int newNum)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Customer not found!");
            }

            customer.NumOfKids = newNum;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method will find an object by the id.
        /// </summary>
        /// <param name="id">The parameter named id which we are looking for. </param>
        /// <returns>Returns a single Customers type record. </returns>
        public override Customers GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// This method removes a record from the database.
        /// </summary>
        /// <param name="id">The parameter is an int.</param>
        public override void Remove(int id)
        {
            Customers obj = this.GetOne(id);
            this.Ctx.Set<Customers>().Remove(obj);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// This method adds a new record to the database.
        /// </summary>
        /// <param name="entity">The parameter is a customer object.</param>
        public override void Insert(Customers entity)
        {
            this.Ctx.Set<Customers>().Add(entity);
            this.Ctx.SaveChanges();
        }
    }
}
