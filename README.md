The application now has a GUI!
=======
The user can add/edit or remove a service option.

Other cool features coming soon with prog4 hf2 and hf3, stay tuned!

last modified: 2021.03.10 @ Tóth Boglárka


BirthdayPartyService
=======

My home project's topic is a birthday party service company. Customers can take orders, in which they define their chosen clown for the party along with the desired services. 
Customers can delete their orders, or modify some details if they remember their own ID.
The company has much more possible modification options.

The program has a DOS-like simple console menu. When the console pops up, the user can enter as the company, or a customer. 

- The company has CRUD menu items, along with the NON-CRUD queries.
- Customers has only access to CRUD menu items.


last modifed: 2020.12.05 @ TothBoglarka
