var class_birthday_party_service_1_1_data_1_1_models_1_1_service_options =
[
    [ "ServiceOptions", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#ac4de90684ac16199136d14c125fdbf50", null ],
    [ "ToString", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#a2165567814ad12c7b1f62f4c6bcdc227", null ],
    [ "ConnectorOrdersServices", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#a309b3ff5a4245eddb8632c0390c20e5b", null ],
    [ "Id", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#a425aac2603927ffbff0767b1cfa40037", null ],
    [ "Indoor", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#a50808dedb78dc5b32e45fa19feaa0450", null ],
    [ "Name", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#a02a39092957d43ec11c242b9e74c4289", null ],
    [ "Price", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#aac0b56c651ff798bd6a804ce60081588", null ],
    [ "Recommendation", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#aca6296e910c8f173bbd4ba59fa51ffe7", null ],
    [ "RecommendedMaxAge", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#a529f8343637f219108b50b6536591211", null ],
    [ "RecommendedMinAge", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html#a31f1d6fc5c2b54e2740215650323b4b9", null ]
];