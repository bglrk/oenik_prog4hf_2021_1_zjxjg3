var class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m =
[
    [ "CopyFrom", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html#a5becb49c970c40a44b2dbfeb3e13ca91", null ],
    [ "Id", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html#a20110eafbf436bdb1383271008732536", null ],
    [ "IsIndoor", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html#a2e300f98304defed448c980281e8ab76", null ],
    [ "MaxAge", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html#a4a3282e7e301f31348dec20ba348dece", null ],
    [ "MinAge", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html#af599deebf790d5169508ed83f52f6484", null ],
    [ "Price", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html#a5fd240f6b76a8e1ffa48bedfed42a36d", null ],
    [ "Recommendation", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html#ab1ce482a7af9f5f653e5342db6ca1481", null ],
    [ "ServiceName", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html#a28717cb6a315a2f02aab84a4876319f5", null ]
];