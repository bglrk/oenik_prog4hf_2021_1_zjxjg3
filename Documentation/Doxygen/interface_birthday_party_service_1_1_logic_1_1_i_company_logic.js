var interface_birthday_party_service_1_1_logic_1_1_i_company_logic =
[
    [ "ChangeClownPrice", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a3a4448948a57c817a1a80e4cd62a0b6c", null ],
    [ "ChangeRecommendation", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a5f57337b924db8c5c5542471224e18be", null ],
    [ "ChangeServiceFull", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a1688117fc7fe6704a31846d11aaba99a", null ],
    [ "ChangeServiceFullBool", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#aaf8cb3180430ce4e6b8bd422951ab907", null ],
    [ "ChangeServiceFullBool2", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a799b8897738e18102c762669664d7fbc", null ],
    [ "ChangeServicePrice", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a1341ffc4603819e882590695aa430e59", null ],
    [ "ChangeStageName", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#abecfdda1797b9992c8be106ad13aaa39", null ],
    [ "ChangeWorkingHours", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a56998a39b30753ec8e4ed7c941f649aa", null ],
    [ "ClownEarnings", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#af6b084607ee30e69acd110b50a9c5a96", null ],
    [ "DeleteServiceBool", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a79961555e2dcfb020dbbf54420ea1c79", null ],
    [ "GetAllCustomers", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a8e8b8733fb3ce8cff7f74e5973ee4218", null ],
    [ "GetAllOrders", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#af216e6b84e972e1eda2887dff899e3e4", null ],
    [ "GetOneCustomer", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#aebc6af3b87b91bc345255bbb61534e4c", null ],
    [ "GetOneOrder", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a10a4eb79b6c70aed76c6f10181e5242c", null ],
    [ "InsertNewService", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a101d8eb82f758904357c3f07bff4cbe5", null ],
    [ "OrderCosts", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#ae93b9c0fe152c72eb8c6ded94e5e00d9", null ],
    [ "WhoDidNotCare", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#ad04a922afa3103db05a984da415782a9", null ]
];