var class_birthday_party_service_1_1_logic_1_1_order_costs_results =
[
    [ "Equals", "class_birthday_party_service_1_1_logic_1_1_order_costs_results.html#adfea2244a664a996183866ac6b52561c", null ],
    [ "GetHashCode", "class_birthday_party_service_1_1_logic_1_1_order_costs_results.html#a1ab629d257e28faa641729d23ff61eb6", null ],
    [ "ToString", "class_birthday_party_service_1_1_logic_1_1_order_costs_results.html#a1416a64e55e7a6d084e415ae3461d095", null ],
    [ "CustomerID", "class_birthday_party_service_1_1_logic_1_1_order_costs_results.html#ab29472e9b582943f2bc3a80f5f512504", null ],
    [ "CustomerName", "class_birthday_party_service_1_1_logic_1_1_order_costs_results.html#ab6a85aae3aced7626433d0ff53bec113", null ],
    [ "TotalPrice", "class_birthday_party_service_1_1_logic_1_1_order_costs_results.html#a2f10b43dabba5cfad9b0b2f8bc15bb00", null ]
];