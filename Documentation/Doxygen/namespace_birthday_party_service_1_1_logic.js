var namespace_birthday_party_service_1_1_logic =
[
    [ "AgeGapResults", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html", "class_birthday_party_service_1_1_logic_1_1_age_gap_results" ],
    [ "ClientLogic", "class_birthday_party_service_1_1_logic_1_1_client_logic.html", "class_birthday_party_service_1_1_logic_1_1_client_logic" ],
    [ "ClownSalaryResults", "class_birthday_party_service_1_1_logic_1_1_clown_salary_results.html", "class_birthday_party_service_1_1_logic_1_1_clown_salary_results" ],
    [ "CompanyLogic", "class_birthday_party_service_1_1_logic_1_1_company_logic.html", "class_birthday_party_service_1_1_logic_1_1_company_logic" ],
    [ "IClientLogic", "interface_birthday_party_service_1_1_logic_1_1_i_client_logic.html", "interface_birthday_party_service_1_1_logic_1_1_i_client_logic" ],
    [ "ICompanyLogic", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic" ],
    [ "IVisibleContentLogic", "interface_birthday_party_service_1_1_logic_1_1_i_visible_content_logic.html", "interface_birthday_party_service_1_1_logic_1_1_i_visible_content_logic" ],
    [ "OrderCostsResults", "class_birthday_party_service_1_1_logic_1_1_order_costs_results.html", "class_birthday_party_service_1_1_logic_1_1_order_costs_results" ],
    [ "VisibleContentLogic", "class_birthday_party_service_1_1_logic_1_1_visible_content_logic.html", "class_birthday_party_service_1_1_logic_1_1_visible_content_logic" ]
];