var hierarchy =
[
    [ "BirthdayPartyService.Logic.AgeGapResults", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html", null ],
    [ "BirthdayPartyService.Web.Controllers.ApiResult", "class_birthday_party_service_1_1_web_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "BirthdayPartyService.WebClient.App", "class_birthday_party_service_1_1_web_client_1_1_app.html", null ],
      [ "BirthdayPartyService.WPF.App", "class_birthday_party_service_1_1_w_p_f_1_1_app.html", null ]
    ] ],
    [ "BirthdayPartyService.Web.Models.BirthdayService", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service.html", null ],
    [ "BirthdayPartyService.Web.Models.BirthdayServiceViewModel", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service_view_model.html", null ],
    [ "BirthdayPartyService.Data.Models.Clowns", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html", null ],
    [ "BirthdayPartyService.Logic.ClownSalaryResults", "class_birthday_party_service_1_1_logic_1_1_clown_salary_results.html", null ],
    [ "BirthdayPartyService.Data.Models.ConnectorOrdersServices", "class_birthday_party_service_1_1_data_1_1_models_1_1_connector_orders_services.html", null ],
    [ "Controller", null, [
      [ "BirthdayPartyService.Web.Controllers.BirthdayServicesApiController", "class_birthday_party_service_1_1_web_1_1_controllers_1_1_birthday_services_api_controller.html", null ],
      [ "BirthdayPartyService.Web.Controllers.BirthdayServicesController", "class_birthday_party_service_1_1_web_1_1_controllers_1_1_birthday_services_controller.html", null ],
      [ "BirthdayPartyService.Web.Controllers.HomeController", "class_birthday_party_service_1_1_web_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "BirthdayPartyService.Data.Models.Customers", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html", null ],
    [ "DbContext", null, [
      [ "BirthdayPartyService.Data.Models.BirthdayPartyServiceDatabaseContext", "class_birthday_party_service_1_1_data_1_1_models_1_1_birthday_party_service_database_context.html", null ]
    ] ],
    [ "BirthdayPartyService.Web.Models.ErrorViewModel", "class_birthday_party_service_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "BirthdayPartyService.WPF.BL.Factory", "class_birthday_party_service_1_1_w_p_f_1_1_b_l_1_1_factory.html", null ],
    [ "BirthdayPartyService.Logic.IClientLogic", "interface_birthday_party_service_1_1_logic_1_1_i_client_logic.html", [
      [ "BirthdayPartyService.Logic.ClientLogic", "class_birthday_party_service_1_1_logic_1_1_client_logic.html", null ]
    ] ],
    [ "BirthdayPartyService.Logic.ICompanyLogic", "interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html", [
      [ "BirthdayPartyService.Logic.CompanyLogic", "class_birthday_party_service_1_1_logic_1_1_company_logic.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "BirthdayPartyService.WPF.UI.EditorWindow", "class_birthday_party_service_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "BirthdayPartyService.WPF.BL.IEditorService", "interface_birthday_party_service_1_1_w_p_f_1_1_b_l_1_1_i_editor_service.html", [
      [ "BirthdayPartyService.WPF.UI.EditorServiceViaWindow", "class_birthday_party_service_1_1_w_p_f_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "BirthdayPartyService.Repo.IRepository< T >", "interface_birthday_party_service_1_1_repo_1_1_i_repository.html", [
      [ "BirthdayPartyService.Repo.Repository< T >", "class_birthday_party_service_1_1_repo_1_1_repository.html", null ]
    ] ],
    [ "BirthdayPartyService.Repo.IRepository< Clowns >", "interface_birthday_party_service_1_1_repo_1_1_i_repository.html", [
      [ "BirthdayPartyService.Repo.IClownsRepository", "interface_birthday_party_service_1_1_repo_1_1_i_clowns_repository.html", [
        [ "BirthdayPartyService.Repo.ClownsRepository", "class_birthday_party_service_1_1_repo_1_1_clowns_repository.html", null ]
      ] ]
    ] ],
    [ "BirthdayPartyService.Repo.IRepository< ConnectorOrdersServices >", "interface_birthday_party_service_1_1_repo_1_1_i_repository.html", [
      [ "BirthdayPartyService.Repo.IConnectorRepository", "interface_birthday_party_service_1_1_repo_1_1_i_connector_repository.html", [
        [ "BirthdayPartyService.Repo.ConnectorRepository", "class_birthday_party_service_1_1_repo_1_1_connector_repository.html", null ]
      ] ]
    ] ],
    [ "BirthdayPartyService.Repo.IRepository< Customers >", "interface_birthday_party_service_1_1_repo_1_1_i_repository.html", [
      [ "BirthdayPartyService.Repo.ICustomersRepository", "interface_birthday_party_service_1_1_repo_1_1_i_customers_repository.html", [
        [ "BirthdayPartyService.Repo.CustomersRepository", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html", null ]
      ] ]
    ] ],
    [ "BirthdayPartyService.Repo.IRepository< Orders >", "interface_birthday_party_service_1_1_repo_1_1_i_repository.html", [
      [ "BirthdayPartyService.Repo.IOrdersRepository", "interface_birthday_party_service_1_1_repo_1_1_i_orders_repository.html", [
        [ "BirthdayPartyService.Repo.OrdersRepository", "class_birthday_party_service_1_1_repo_1_1_orders_repository.html", null ]
      ] ]
    ] ],
    [ "BirthdayPartyService.Repo.IRepository< ServiceOptions >", "interface_birthday_party_service_1_1_repo_1_1_i_repository.html", [
      [ "BirthdayPartyService.Repo.IServicesRepository", "interface_birthday_party_service_1_1_repo_1_1_i_services_repository.html", [
        [ "BirthdayPartyService.Repo.ServicesRepository", "class_birthday_party_service_1_1_repo_1_1_services_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "BirthdayPartyService.WebClient.MyIOC", "class_birthday_party_service_1_1_web_client_1_1_my_i_o_c.html", null ],
      [ "BirthdayPartyService.WPF.MyIoc", "class_birthday_party_service_1_1_w_p_f_1_1_my_ioc.html", null ]
    ] ],
    [ "BirthdayPartyService.WPF.BL.IServiceLogic", "interface_birthday_party_service_1_1_w_p_f_1_1_b_l_1_1_i_service_logic.html", [
      [ "BirthdayPartyService.WPF.BL.ServiceLogic", "class_birthday_party_service_1_1_w_p_f_1_1_b_l_1_1_service_logic.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "BirthdayPartyService.WPF.UI.IndoorBooleanToStringConverter", "class_birthday_party_service_1_1_w_p_f_1_1_u_i_1_1_indoor_boolean_to_string_converter.html", null ]
    ] ],
    [ "BirthdayPartyService.Logic.IVisibleContentLogic", "interface_birthday_party_service_1_1_logic_1_1_i_visible_content_logic.html", [
      [ "BirthdayPartyService.Logic.VisibleContentLogic", "class_birthday_party_service_1_1_logic_1_1_visible_content_logic.html", null ]
    ] ],
    [ "BirthdayPartyService.WebClient.MainLogic", "class_birthday_party_service_1_1_web_client_1_1_main_logic.html", null ],
    [ "BirthdayPartyService.Web.Models.MapperFactory", "class_birthday_party_service_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "ObservableObject", null, [
      [ "BirthdayPartyService.WebClient.BirthdayServiceVM", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html", null ],
      [ "BirthdayPartyService.WPF.Data.BirthdayService", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html", null ]
    ] ],
    [ "BirthdayPartyService.Logic.OrderCostsResults", "class_birthday_party_service_1_1_logic_1_1_order_costs_results.html", null ],
    [ "BirthdayPartyService.Data.Models.Orders", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html", null ],
    [ "BirthdayPartyService.Web.Program", "class_birthday_party_service_1_1_web_1_1_program.html", null ],
    [ "RazorPage", null, [
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views_BirthdayServices_BirthdayServicesDetails", "class_asp_net_core_1_1_views___birthday_services___birthday_services_details.html", null ],
      [ "AspNetCore.Views_BirthdayServices_BirthdayServicesEdit", "class_asp_net_core_1_1_views___birthday_services___birthday_services_edit.html", null ],
      [ "AspNetCore.Views_BirthdayServices_BirthdayServicesIndex", "class_asp_net_core_1_1_views___birthday_services___birthday_services_index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ]
    ] ],
    [ "RazorPage< IEnumerable< BirthdayPartyService.Web.Models.BirthdayService >>", null, [
      [ "AspNetCore.Views_BirthdayServices_BirthdayServicesList", "class_asp_net_core_1_1_views___birthday_services___birthday_services_list.html", null ]
    ] ],
    [ "BirthdayPartyService.Repo.Repository< Clowns >", "class_birthday_party_service_1_1_repo_1_1_repository.html", [
      [ "BirthdayPartyService.Repo.ClownsRepository", "class_birthday_party_service_1_1_repo_1_1_clowns_repository.html", null ]
    ] ],
    [ "BirthdayPartyService.Repo.Repository< ConnectorOrdersServices >", "class_birthday_party_service_1_1_repo_1_1_repository.html", [
      [ "BirthdayPartyService.Repo.ConnectorRepository", "class_birthday_party_service_1_1_repo_1_1_connector_repository.html", null ]
    ] ],
    [ "BirthdayPartyService.Repo.Repository< Customers >", "class_birthday_party_service_1_1_repo_1_1_repository.html", [
      [ "BirthdayPartyService.Repo.CustomersRepository", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html", null ]
    ] ],
    [ "BirthdayPartyService.Repo.Repository< Orders >", "class_birthday_party_service_1_1_repo_1_1_repository.html", [
      [ "BirthdayPartyService.Repo.OrdersRepository", "class_birthday_party_service_1_1_repo_1_1_orders_repository.html", null ]
    ] ],
    [ "BirthdayPartyService.Repo.Repository< ServiceOptions >", "class_birthday_party_service_1_1_repo_1_1_repository.html", [
      [ "BirthdayPartyService.Repo.ServicesRepository", "class_birthday_party_service_1_1_repo_1_1_services_repository.html", null ]
    ] ],
    [ "BirthdayPartyService.Data.Models.ServiceOptions", "class_birthday_party_service_1_1_data_1_1_models_1_1_service_options.html", null ],
    [ "SimpleIoc", null, [
      [ "BirthdayPartyService.WebClient.MyIOC", "class_birthday_party_service_1_1_web_client_1_1_my_i_o_c.html", null ],
      [ "BirthdayPartyService.WPF.MyIoc", "class_birthday_party_service_1_1_w_p_f_1_1_my_ioc.html", null ]
    ] ],
    [ "BirthdayPartyService.Web.Startup", "class_birthday_party_service_1_1_web_1_1_startup.html", null ],
    [ "ViewModelBase", null, [
      [ "BirthdayPartyService.WebClient.MainVM", "class_birthday_party_service_1_1_web_client_1_1_main_v_m.html", null ],
      [ "BirthdayPartyService.WPF.VM.EditorViewModel", "class_birthday_party_service_1_1_w_p_f_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "BirthdayPartyService.WPF.VM.MainViewModel", "class_birthday_party_service_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "BirthdayPartyService.WPF.UI.EditorWindow", "class_birthday_party_service_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "BirthdayPartyService.WebClient.EditorWindow", "class_birthday_party_service_1_1_web_client_1_1_editor_window.html", null ],
      [ "BirthdayPartyService.WebClient.MainWindow", "class_birthday_party_service_1_1_web_client_1_1_main_window.html", null ],
      [ "BirthdayPartyService.WPF.MainWindow", "class_birthday_party_service_1_1_w_p_f_1_1_main_window.html", null ]
    ] ]
];