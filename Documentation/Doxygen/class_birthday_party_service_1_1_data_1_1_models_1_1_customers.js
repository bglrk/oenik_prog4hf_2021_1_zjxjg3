var class_birthday_party_service_1_1_data_1_1_models_1_1_customers =
[
    [ "Customers", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a4fdbb14a52c1750f4d30f5e9f96dd2db", null ],
    [ "ToString", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a6aefd8e75a0b1cd4402e06f51a13f976", null ],
    [ "Address", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a6340963432088805c8bc0376e8aad04d", null ],
    [ "City", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a983656c4592bcc71e7543c606b501e86", null ],
    [ "Email", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a30b78dc9519d9859352f57b5eb6ba52a", null ],
    [ "Id", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#ac40a3711bc52bafcc98b35b0d7087109", null ],
    [ "KidAge", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a44cd242b68c3fb8d5c1b0413d87d1119", null ],
    [ "Name", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a32288287c5ac22c018d19b5dde3d7ddf", null ],
    [ "NumOfKids", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a80000163cea96a3b0caf8a24e60a3c33", null ],
    [ "Orders", "class_birthday_party_service_1_1_data_1_1_models_1_1_customers.html#a41b289b738f70e0e42fe5ca184b27717", null ]
];