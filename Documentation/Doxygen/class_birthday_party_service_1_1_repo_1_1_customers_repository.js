var class_birthday_party_service_1_1_repo_1_1_customers_repository =
[
    [ "CustomersRepository", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html#a825ec41e65bfe1ffb054e71f396fe27f", null ],
    [ "ChangeAddress", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html#a02c518ba3441a130e34b9cb1b03ef464", null ],
    [ "ChangeEmail", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html#aee56c7c460fdf9f3e931c3476558946a", null ],
    [ "ChangeNumOfKids", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html#a69c126e289ab31d37a88aca083028f06", null ],
    [ "GetOne", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html#a289ad7cea41585043aeafd482347ecec", null ],
    [ "Insert", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html#ac0b40ec19892901558b87a37b422bb5e", null ],
    [ "Remove", "class_birthday_party_service_1_1_repo_1_1_customers_repository.html#ab366fe7ea737974b91d94323adb3835d", null ]
];