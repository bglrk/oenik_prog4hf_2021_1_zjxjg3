var class_birthday_party_service_1_1_logic_1_1_client_logic =
[
    [ "ClientLogic", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#a1b30cc03d7264e4e1145f708be4d298d", null ],
    [ "ChangeCustomerAddress", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#a10cbb8d386323382a37323cb569ebab8", null ],
    [ "ChangeCustomerEmail", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#ab893cf0081d9f4337efa9417f97585ae", null ],
    [ "ChangeNumOfKids", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#aa0a610e6cc430ca09fbcbc51fcdab30f", null ],
    [ "ChangeOrderDate", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#a394e4ff209722b1c34cad7a33c4cb722", null ],
    [ "DeleteConnection", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#af8e587b6c5d17beff53152983ea4e429", null ],
    [ "DeleteCustomer", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#a884e50ffd3b6948634380508377b9f8d", null ],
    [ "DeleteOrder", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#a3cb978ab221ecabc1684323cd4372dfa", null ],
    [ "InsertNewConnection", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#a8dd378f4ff596d0b0c4560bfe1127c27", null ],
    [ "InsertNewCustomer", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#a1157fecd672db795fc93e11f6ef9b4e5", null ],
    [ "InsertNewOrder", "class_birthday_party_service_1_1_logic_1_1_client_logic.html#acfb7141665824cea59dab82fbfcab50c", null ]
];