var class_birthday_party_service_1_1_data_1_1_models_1_1_clowns =
[
    [ "Clowns", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#a8806e75c5c7d9643a89ffc185d25e772", null ],
    [ "ToString", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#a08b433c8ec45fc289e8b89c84d1e71bd", null ],
    [ "Duration", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#a0f033d1911eb338fe5e95926745f4d47", null ],
    [ "Gender", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#a77b0f2769c2693ae9e4bcfe752ae574f", null ],
    [ "Id", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#ad1d4169f534706484a66e41d1b5a84eb", null ],
    [ "Name", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#a8a0c9093e1bf8de05336f62afcf8c4f2", null ],
    [ "Orders", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#ade40992808ab9b5e9e681fda6a1fc81c", null ],
    [ "Price", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#a1a47c0171358b6a19f025001597579c6", null ],
    [ "StageName", "class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#a5ffb929926e9e8a411009535f55897d5", null ]
];