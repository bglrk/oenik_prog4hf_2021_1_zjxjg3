var class_birthday_party_service_1_1_repo_1_1_services_repository =
[
    [ "ServicesRepository", "class_birthday_party_service_1_1_repo_1_1_services_repository.html#ae9eb7d0c61a82ee765507cf5387e5694", null ],
    [ "ChangePrice", "class_birthday_party_service_1_1_repo_1_1_services_repository.html#a7cda25c98868a2842ecad7af1ee32e3a", null ],
    [ "GetOne", "class_birthday_party_service_1_1_repo_1_1_services_repository.html#a19592f30ddd5e79084d02f80133a5a5f", null ],
    [ "Insert", "class_birthday_party_service_1_1_repo_1_1_services_repository.html#a244cf1f884c33a541de013172c361767", null ],
    [ "Remove", "class_birthday_party_service_1_1_repo_1_1_services_repository.html#a3c61aea899ebd6cf3d95339181fa62c1", null ],
    [ "UpdateRecommendation", "class_birthday_party_service_1_1_repo_1_1_services_repository.html#a8f4cc6c4f42bbcc0c43ad2e63a5d5e13", null ],
    [ "UpdateServiceFull", "class_birthday_party_service_1_1_repo_1_1_services_repository.html#a7a35f1a0b1afde19f5e6df8a4dcf17eb", null ]
];