var class_birthday_party_service_1_1_logic_1_1_company_logic =
[
    [ "CompanyLogic", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#ae7905e865b9f4f7bcadc1935c8b63f9f", null ],
    [ "ChangeClownPrice", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#abb0e373e335a6ed974799c07c879be6a", null ],
    [ "ChangeRecommendation", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a1be2c8e89336298cdc0fdee69c14f69f", null ],
    [ "ChangeServiceFull", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a667b9317e91a1087546ae857e0d37bad", null ],
    [ "ChangeServiceFullBool", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a50f937051aca22d5c846a45b66af8f8b", null ],
    [ "ChangeServiceFullBool2", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#ab9464f2994e182bda077fcf4180fa71f", null ],
    [ "ChangeServicePrice", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a90a1f2486811f83143c4f5bad3cc4113", null ],
    [ "ChangeStageName", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a18a88b9f2503f49bbcf1e834385a72a3", null ],
    [ "ChangeWorkingHours", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a2a5fad60e890101504c0aebf9e1d41cb", null ],
    [ "ClownEarnings", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#ab5fdfad4582cfe7b917c3aa199519074", null ],
    [ "ClownEarningsAsync", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#ad076bdd5a18aeac75465b247fc80e887", null ],
    [ "DeleteClown", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a4d985ffeaca5056129f8a801f1e418bb", null ],
    [ "DeleteService", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a0f6c5d8a5e3a932d59cb9a2280090888", null ],
    [ "DeleteServiceBool", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a5f76e509f290e7297ec93ef6c59c7680", null ],
    [ "GetAllConnections", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a7b19f3358a33a072e23d52a90d89db2b", null ],
    [ "GetAllCustomers", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#ab3e9231bfe15fbfd30df82c47dcd9dc2", null ],
    [ "GetAllOrders", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a0eaa8a2e7444afe10dd61858f5e59a45", null ],
    [ "GetOneConnection", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a99f5120cbf55d0fca0c88cfb2c8d3d6d", null ],
    [ "GetOneCustomer", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#ab5b26d2035e93458a10134f1912a4da6", null ],
    [ "GetOneOrder", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a6d7aa4725b0f18b38a010ba43581e606", null ],
    [ "InsertNewClown", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a1bddf34a45e825ae0a97dcf99d62c771", null ],
    [ "InsertNewService", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a0cb912a9a9a48150279935b815b3dece", null ],
    [ "OrderCosts", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a7c5918e63c725a65f770fb3556862064", null ],
    [ "OrderCostsAsync", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a45c8d073bc66f1fe5d591b0a5b621c9e", null ],
    [ "WhoDidNotCare", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#ade566c1957cd25f7818fc533dce73b3e", null ],
    [ "WhoDidNotCareAsync", "class_birthday_party_service_1_1_logic_1_1_company_logic.html#a1c16dd5ccebfd125fdd0dc9ee7e5ba12", null ]
];