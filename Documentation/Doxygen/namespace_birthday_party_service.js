var namespace_birthday_party_service =
[
    [ "Data", "namespace_birthday_party_service_1_1_data.html", "namespace_birthday_party_service_1_1_data" ],
    [ "Logic", "namespace_birthday_party_service_1_1_logic.html", "namespace_birthday_party_service_1_1_logic" ],
    [ "Repo", "namespace_birthday_party_service_1_1_repo.html", "namespace_birthday_party_service_1_1_repo" ],
    [ "Web", "namespace_birthday_party_service_1_1_web.html", "namespace_birthday_party_service_1_1_web" ],
    [ "WebClient", "namespace_birthday_party_service_1_1_web_client.html", "namespace_birthday_party_service_1_1_web_client" ],
    [ "WPF", "namespace_birthday_party_service_1_1_w_p_f.html", "namespace_birthday_party_service_1_1_w_p_f" ]
];