var class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service =
[
    [ "Id", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service.html#a401e979729813668b3d6916559d0d8bf", null ],
    [ "IsIndoor", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service.html#a24ffb93eb2229ac9ee73eca0e77f3816", null ],
    [ "MaxAge", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service.html#ad6ecae206aad0c6568cf8687bcaee7c0", null ],
    [ "MinAge", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service.html#a359e053a31fb4a2cbe0fccf151d23284", null ],
    [ "Price", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service.html#af4b520af13ad9f07dd8c469fd57644b8", null ],
    [ "Recommendation", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service.html#af63603bec5c0e74932e71da972b8e120", null ],
    [ "ServiceName", "class_birthday_party_service_1_1_web_1_1_models_1_1_birthday_service.html#a8506312eac630c50a2281c4e601443b9", null ]
];