var namespace_birthday_party_service_1_1_web_client =
[
    [ "App", "class_birthday_party_service_1_1_web_client_1_1_app.html", "class_birthday_party_service_1_1_web_client_1_1_app" ],
    [ "BirthdayServiceVM", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m.html", "class_birthday_party_service_1_1_web_client_1_1_birthday_service_v_m" ],
    [ "EditorWindow", "class_birthday_party_service_1_1_web_client_1_1_editor_window.html", "class_birthday_party_service_1_1_web_client_1_1_editor_window" ],
    [ "MainLogic", "class_birthday_party_service_1_1_web_client_1_1_main_logic.html", "class_birthday_party_service_1_1_web_client_1_1_main_logic" ],
    [ "MainVM", "class_birthday_party_service_1_1_web_client_1_1_main_v_m.html", "class_birthday_party_service_1_1_web_client_1_1_main_v_m" ],
    [ "MainWindow", "class_birthday_party_service_1_1_web_client_1_1_main_window.html", "class_birthday_party_service_1_1_web_client_1_1_main_window" ],
    [ "MyIOC", "class_birthday_party_service_1_1_web_client_1_1_my_i_o_c.html", "class_birthday_party_service_1_1_web_client_1_1_my_i_o_c" ]
];