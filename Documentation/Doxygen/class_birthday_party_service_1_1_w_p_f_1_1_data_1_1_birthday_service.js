var class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service =
[
    [ "CopyFrom", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html#aa98bd113b41ab562c339c6835736a974", null ],
    [ "Id", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html#a89655c36bbec57fe34cba62aed79371c", null ],
    [ "IsIndoor", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html#aaabaca378a8e122d74c4a5eda923a986", null ],
    [ "MaxAge", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html#a3b03b0f15525bdd95a3c42c7b2a1a5e0", null ],
    [ "MinAge", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html#a4a1e63cbd377842db6143f95819c0d14", null ],
    [ "Name", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html#aeca7ecf14325bd353c44caf768776cae", null ],
    [ "Price", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html#a17f87961064d48ebb46cb7cd527048cd", null ],
    [ "Recommendation", "class_birthday_party_service_1_1_w_p_f_1_1_data_1_1_birthday_service.html#a5b9f47d5f3467272ba084cc7da794389", null ]
];