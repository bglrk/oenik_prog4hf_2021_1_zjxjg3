var searchData=
[
  ['datetime_80',['DateTime',['../class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a53f479d7febeb1f6789844800d893031',1,'BirthdayPartyService::Data::Models::Orders']]],
  ['delcmd_81',['DelCmd',['../class_birthday_party_service_1_1_web_client_1_1_main_v_m.html#a7eb699813f17f8f36e6252215086e491',1,'BirthdayPartyService::WebClient::MainVM']]],
  ['deleteclown_82',['DeleteClown',['../class_birthday_party_service_1_1_logic_1_1_company_logic.html#a4d985ffeaca5056129f8a801f1e418bb',1,'BirthdayPartyService::Logic::CompanyLogic']]],
  ['deletecmd_83',['DeleteCmd',['../class_birthday_party_service_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a92735c831a1a4f167f381f83239854b1',1,'BirthdayPartyService::WPF::VM::MainViewModel']]],
  ['deleteconnection_84',['DeleteConnection',['../class_birthday_party_service_1_1_logic_1_1_client_logic.html#af8e587b6c5d17beff53152983ea4e429',1,'BirthdayPartyService::Logic::ClientLogic']]],
  ['deletecustomer_85',['DeleteCustomer',['../class_birthday_party_service_1_1_logic_1_1_client_logic.html#a884e50ffd3b6948634380508377b9f8d',1,'BirthdayPartyService::Logic::ClientLogic']]],
  ['deleteorder_86',['DeleteOrder',['../class_birthday_party_service_1_1_logic_1_1_client_logic.html#a3cb978ab221ecabc1684323cd4372dfa',1,'BirthdayPartyService::Logic::ClientLogic']]],
  ['deleteservice_87',['DeleteService',['../class_birthday_party_service_1_1_logic_1_1_company_logic.html#a0f6c5d8a5e3a932d59cb9a2280090888',1,'BirthdayPartyService.Logic.CompanyLogic.DeleteService()'],['../interface_birthday_party_service_1_1_w_p_f_1_1_b_l_1_1_i_service_logic.html#a169ccb7b4a224b87880edc2e8cab407c',1,'BirthdayPartyService.WPF.BL.IServiceLogic.DeleteService()'],['../class_birthday_party_service_1_1_w_p_f_1_1_b_l_1_1_service_logic.html#af0feb562d7ab75f288971b22408201d4',1,'BirthdayPartyService.WPF.BL.ServiceLogic.DeleteService()']]],
  ['deleteservicebool_88',['DeleteServiceBool',['../class_birthday_party_service_1_1_logic_1_1_company_logic.html#a5f76e509f290e7297ec93ef6c59c7680',1,'BirthdayPartyService.Logic.CompanyLogic.DeleteServiceBool()'],['../interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#a79961555e2dcfb020dbbf54420ea1c79',1,'BirthdayPartyService.Logic.ICompanyLogic.DeleteServiceBool()']]],
  ['deloneservice_89',['DelOneService',['../class_birthday_party_service_1_1_web_1_1_controllers_1_1_birthday_services_api_controller.html#a75ac19a222e83fbb612d00b93b6758f3',1,'BirthdayPartyService::Web::Controllers::BirthdayServicesApiController']]],
  ['details_90',['Details',['../class_birthday_party_service_1_1_web_1_1_controllers_1_1_birthday_services_controller.html#a4fe118eea08ae130e35291413c816ad7',1,'BirthdayPartyService::Web::Controllers::BirthdayServicesController']]],
  ['duration_91',['Duration',['../class_birthday_party_service_1_1_data_1_1_models_1_1_clowns.html#a0f033d1911eb338fe5e95926745f4d47',1,'BirthdayPartyService::Data::Models::Clowns']]]
];
