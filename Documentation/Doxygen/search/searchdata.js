var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvwx",
  1: "abcefghimoprsv",
  2: "abx",
  3: "abcdefghimoprstuvw",
  4: "s",
  5: "abcdegiklmnoprstv",
  6: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Pages"
};

