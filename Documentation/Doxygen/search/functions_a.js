var searchData=
[
  ['onconfiguring_401',['OnConfiguring',['../class_birthday_party_service_1_1_data_1_1_models_1_1_birthday_party_service_database_context.html#a7b29dda07e714cd9fe8ee77b81ce554a',1,'BirthdayPartyService::Data::Models::BirthdayPartyServiceDatabaseContext']]],
  ['onmodelcreating_402',['OnModelCreating',['../class_birthday_party_service_1_1_data_1_1_models_1_1_birthday_party_service_database_context.html#ad037816eb04bdbf0e31bac22e3645674',1,'BirthdayPartyService::Data::Models::BirthdayPartyServiceDatabaseContext']]],
  ['ordercosts_403',['OrderCosts',['../class_birthday_party_service_1_1_logic_1_1_company_logic.html#a7c5918e63c725a65f770fb3556862064',1,'BirthdayPartyService.Logic.CompanyLogic.OrderCosts()'],['../interface_birthday_party_service_1_1_logic_1_1_i_company_logic.html#ae93b9c0fe152c72eb8c6ded94e5e00d9',1,'BirthdayPartyService.Logic.ICompanyLogic.OrderCosts()']]],
  ['ordercostsasync_404',['OrderCostsAsync',['../class_birthday_party_service_1_1_logic_1_1_company_logic.html#a45c8d073bc66f1fe5d591b0a5b621c9e',1,'BirthdayPartyService::Logic::CompanyLogic']]],
  ['orders_405',['Orders',['../class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a08b2d088b6d1a5f70c6c81818e52e362',1,'BirthdayPartyService::Data::Models::Orders']]],
  ['ordersrepository_406',['OrdersRepository',['../class_birthday_party_service_1_1_repo_1_1_orders_repository.html#afc20f3a8ae42d483ceddd604c539d90f',1,'BirthdayPartyService::Repo::OrdersRepository']]]
];
