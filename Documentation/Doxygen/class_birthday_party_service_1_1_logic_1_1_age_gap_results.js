var class_birthday_party_service_1_1_logic_1_1_age_gap_results =
[
    [ "Equals", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#a47a9c46032ed103b01d951c0c24b3f94", null ],
    [ "GetHashCode", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#a0184202235113be800af04be14aa445c", null ],
    [ "ToString", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#a9cf4ea12aaa13af558bbfb04230c11ed", null ],
    [ "ChosenService", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#a49fe784953924c256e171c7891644fa2", null ],
    [ "CustomerID", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#a925c22a2728ded4b2e54499bea67cfe4", null ],
    [ "CustomerName", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#af072953dd8eb3da32459f6fb090d56c0", null ],
    [ "KidAge", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#a8f1806aa1a95f1b3e03f5138b202b0d5", null ],
    [ "RecommendedMaxAge", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#a0ce76408f4e74fc8a26454f2144ba9f4", null ],
    [ "RecommendedMinAge", "class_birthday_party_service_1_1_logic_1_1_age_gap_results.html#a16f2050af0d1920fbdfa4f83d809088c", null ]
];