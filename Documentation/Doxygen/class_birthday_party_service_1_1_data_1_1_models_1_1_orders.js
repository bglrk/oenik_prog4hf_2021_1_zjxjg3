var class_birthday_party_service_1_1_data_1_1_models_1_1_orders =
[
    [ "Orders", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a08b2d088b6d1a5f70c6c81818e52e362", null ],
    [ "ToString", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a3954a1b700867a98c9f99fe89df9388a", null ],
    [ "Clown", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#adb9c48b931c395adf85e16f4df961a80", null ],
    [ "ClownId", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a47259c1e33fae25fdb0f38b2c4009653", null ],
    [ "ConnectorOrdersServices", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a80337c2d476e7e427aeb4a3fac67c4c8", null ],
    [ "Customer", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#aef6c9697e35bd5e2535c6f65e16c2740", null ],
    [ "CustomerId", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a63709edbef81fed1a31f0eaf1adf2623", null ],
    [ "DateTime", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a53f479d7febeb1f6789844800d893031", null ],
    [ "Id", "class_birthday_party_service_1_1_data_1_1_models_1_1_orders.html#a3173666a47ffc649a6cab274c2839936", null ]
];